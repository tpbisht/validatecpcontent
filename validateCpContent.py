import csv
import pandas as pd
import numpy as np
import logging
import re
import datetime
from urlparse import urlparse
import requests
import sys
from os import path
import os
from dateutil.parser import parse
from datetime import date
import boto3
import botocore
import time

reload(sys)
sys.setdefaultencoding('utf8')

working_dir     = os.getcwd()
access_key      = ''
secret_key      = ''
files_location  = 's3'
delayCopy       = 1
dryRun          = True
airtelApproval  = False
query_column_no = 22
destination_bucket       = 'content-partner.tv.stream.com'
ingestion_trigger_bucket = 'alpha.vod-cp'
#ingestion_trigger_bucket = 'content-partner.tv.stream.com'
release_date_oldest_content_year = 1960
old_version_support_date         = '31/12/2020'
allowed_duration_in_seconds      = 18000
allowed_min_duration_in_seconds  = 10
old_format_file                  = False


# Random number generator 
from random import seed
from random import randint
random_number = randint(100000, 999999)
now = datetime.datetime.now()


filename = ''
logger_mode = 'INFO'
array_of_successsfully_validated_resources = []
array_of_successsfully_validated_rows = []
array_of_failure_rows = []
global failureReasonForThisRow



allowed_content_type          = ('Movie', 'TvShow', 'Series', 'Video', 'TV Show')
series_category               = ['Series', 'TvShow', 'TV Show']
parental_guide_allowed_values = ('G','PG','13','15', '18', '18+', '16')
supported_movie_formats       = ['mp4']
supported_image_formats       = ('jpg','jpeg','png')
special_chars_not_allowed     = "[@!#$%^&*()<>?/\|}{~:-]"
supported_country_list        = ('KE','GA','TD','TZ','CG','ZM','MG','UG','SC','MW','NG','CD','CF','NE','RW','IN','US','LL','CND')
#supported_genre_list          = ('Documentary','Gospel','Action','Adventure','Comedy','Crime','Drama','Fantasy','Historical','Horror','Mystery','Fiction','Philosophical','Political','Romance','Saga','Satire','SiFi','Social','Speculative','Thriller','Urban','Western','Animation','Live-action')
supported_genre_list          = ('Action & Adventure','Anime & Manga','Arts & Entertainment','Beauty & Fashion','Comedy','Documentary','Drama','Educational','Exercise & Fitness','Faith & Spirituality', 'Fantasy','Food & Drink', 'Foreign Films', 'Gaming', 'Health', 'Home & Garden', 'Horror', 'Kids & Family', 'LGBT', 'Military & War', 'Music Videos & Concerts', 'Musicals', 'Mystery & Thrillers', 'Performing Arts', 'Reality TV', 'Romance', 'Science Fiction', 'Soap Operas', 'Special Interests', 'Sports', 'TV Game Shows', 'TV News Programming', 'TV Talk Shows','Westerns')
quality_rating_list           = ('Basic','Basic Channel','Premium','Premium Channel')
tvod_allowed_list             = ('True','False','TRUE','FALSE')
subtitles_allowed_countries   = ('en','fr')
query_allowed_values          = ['Ingestion','Correction_VD','Correction_TR','Correction_NM']

# Added after comments
# with following we will have control to randomly add or delete values from the list
# in case we do not want to push enforcements in any columns

columns_mandatory_for_ingestion        = ('Content ID', 'Title', 'Content Type', 'Season Number', 'Episode Number', 'Description', 'Credits/Cast/Artiste(s)', 'Genre', 'Release Date', 'Duration','Language', 'Content Producer', 'Parental Guidance Rating', 'ResourceFile', 'Subtitles','Trailer', 'Landscape_4x3', 'Portrait_2x3', 'Landscape_16x9', 'TVOD', 'Quality Rating', 'Query')
columns_to_not_have_newline_characters = columns_mandatory_for_ingestion
query_ingestion_list                   = ('Content ID', 'Title', 'Content Type', 'Season Number', 'Episode Number', 'Description', 'Credits/Cast/Artiste(s)', 'Genre', 'Release Date', 'Duration','Language', 'Content Producer', 'Parental Guidance Rating', 'ResourceFile', 'Subtitles', 'Landscape_4x3', 'Portrait_2x3', 'Landscape_16x9', 'Quality Rating', 'Query')
query_correction_vd_list               = ('Content ID', 'ResourceFile')
query_correction_tr_list               = ('Content ID', 'Trailer')
query_correction_nm_list               = ('Content ID', 'Title', 'Content Type', 'Season Number', 'Episode Number', 'Description', 'Credits/Cast/Artiste(s)', 'Genre', 'Release Date', 'Duration','Language', 'Content Producer', 'Parental Guidance Rating', 'Subtitles', 'Landscape_4x3', 'Portrait_2x3', 'Landscape_16x9', 'Quality Rating', 'Query')
columns_requiring_validation           = query_ingestion_list
comma_supported_feilds                 = ['Description','Credits/Cast/Artiste(s)','Genre','Language','Content Producer']
double_quotes_not_supported_feilds     = ['Description']


at_least_one_failure   = False

# Functions beyond this point
def getLogger():
	##_____ Logging configuration _____ ##
	logFileName = 'validateCpContent_' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-' + str(now.hour) + '-' + str(now.minute) + '-' + str(now.second) + '.log'
	logger = logging.getLogger('cp_validator') # create logger
	logger.setLevel(logging.DEBUG) # create console handler and set level to debug
	#logging.basicConfig(filename=logFileName,level=logging.DEBUG)
	logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',filename=logFileName,level=logging.DEBUG)
	#format='%(asctime)s %(message)s'
	
	ch = logging.StreamHandler()
	ch.setLevel(logger_mode)
	# create formatter
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	# add formatter to ch
	ch.setFormatter(formatter)
	# add ch to logger
	logger.addHandler(ch)
	return logger

def list_special_characters ( list ):
	str = ''
	for value in list:
		str = str + value + '/'
	str = str[:-1]
	return str

def isPresent_newline_char ( string ):
	string = str(string)
	if ( "\n" in string ):
		logger.debug('The string (' + string + ') contains a new line character')
		return True
	else:
		logger.debug('The string (' + string + ') does not contain any newline character')
		return False

# Function checks if the string 
# contains any special character 
def isPresent_special_char ( string ): 
    # Make own character set and pass  
    # this as argument in compile method 
    regex = re.compile(special_chars_not_allowed) 
      
    # Pass the string in search  
    # method of regex object.     
    #print '______',string.isspace()
    if (regex.search(string) == None and not bool(re.search(r"\s", string))):
    	logger.debug('The string (' + string + ') does not contain any of the special characters like (' + special_chars_not_allowed + ')')
    	return False
    else:
    	logger.debug('The string (' + string + ') either contains spaces or contains one of the special characters like (' + special_chars_not_allowed + ')')
    	return True

def isPresent_special_char_space_allowed ( string ): 
    # Make own character set and pass  
    # this as argument in compile method 
    regex = re.compile(special_chars_not_allowed) 
      
    if (regex.search(string) == None ):
    	logger.debug('The string (' + string + ') does not contain any of the special characters like (' + special_chars_not_allowed + ')')
    	return False
    else:
    	logger.debug('The string (' + string + ') does not contain any of the special characters like (' + special_chars_not_allowed + ')')
    	return True

def isStartingWithLetters ( string ):
	#if re.match('^[a-zA-Z][a-zA-Z]+.*[0-9]+$', string ) :
	if re.match('^[a-zA-Z]', string ) :
		return True
	else:
		return False

def is_Null ( value ) :
	if not value:
		return 1
	else:
		return 0

def getCurrentDateTime () :
	return str(now.strftime("%Y%m%d_%H%M%S"))

def validate_column_heading ( column_list, no_of_columns ):

	global column_sequence_valid
	logger.debug('Total no of columns in filename (' + str(filename) + ') is : ' + str(no_of_columns))
	
	for column_name in column_list:
		if re.search( 'Unnamed:', column_name ):
			continue
		logger.debug('DEBUG: Checking for column (' + column_name + '), Value from array is ('+ str(columns_mandatory_for_ingestion) +')')
		if column_name.encode('ascii','ignore').lower() not in (qr.lower() for qr in columns_mandatory_for_ingestion):
			column_sequence_valid = False
			at_least_one_failure = True
			logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
			logger.info('VALIDATION FAILED: Column heading (' + column_name + ') is not proper and should be one of : (' + list_allowed_headers(columns_mandatory_for_ingestion) + ')')
			
		else:
			logger.debug('Column heading (' + column_name + ') has a heading as expected')
	
	logger.debug('Validating if columns are correctly placed in order')
	for i in range(no_of_columns):
		if ( column_list[i].encode('ascii','ignore') != columns_mandatory_for_ingestion[i]):
			column_sequence_valid = False
			logger.info('VALIDATION FAILED: At column no (' + str(i+1)+ '), column heading is (' + column_list[i] + ') but expected heading is (' + columns_mandatory_for_ingestion[i] + ')')
			at_least_one_failure = True
			logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
		else: 
			logger.debug('Validation Pass: At column no (' + str(i+1) + '), column heading (' + column_list[i] + ') is placed as expected')

def validate_content_id ( local_content_id, local_content_id_column_heading ):
	global content_id_valid
	global content_id
	global at_least_one_failure
	content_id = local_content_id
	global content_id_column_heading
	global failureReasonForThisRow
	content_id_column_heading = local_content_id_column_heading

	
	# Debug statement 
	logger.debug('Validating ' + str(content_id_column_heading) + ' (' + str(content_id) + ')')

	if isinstance(content_id, unicode):
		content_id       = str(content_id.strip())
		logger.debug( content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( content_id_column_heading not in columns_mandatory_for_ingestion ) :
			logger.info('VALIDATION FAILED: ' + content_id_column_heading + ' (' + content_id + ') is mandatory field. Please update script config to add it in the list of mandatory felds')
			sys.exit(1)
		else:
			# Should not be null if mandatory feild 
			#if not content_id:
			#	content_id_valid = False
			#	logger.info('VALIDATION FAILED: Found content id is NULL. It should not be NULL')

			# Should be unique across CP

			# Should not have any special characters
			if isPresent_special_char(content_id):
				logger.info('VALIDATION FAILED: Found either spaces or special characters in ' + content_id_column_heading + ' (' + content_id + '). It should not contain spaces or any of (' + special_chars_not_allowed + ')')
				failureReasonForThisRow += 'Found either spaces or special characters in ' + content_id_column_heading + '|'
				content_id_valid = False
			else:
				logger.debug('Validation Pass: The content ID (' + content_id + ') does not have any special character (' + special_chars_not_allowed + ')')

			if ( content_id_column_heading in columns_to_not_have_newline_characters ) :
				if isPresent_newline_char(content_id):
					logger.info('VALIDATION FAILED: ' + content_id_column_heading + ' (' + content_id + ') is having a new line character which is not allowed')
					failureReasonForThisRow += content_id_column_heading + ' is having a new line character which is not allowed' + '|'
					content_id_valid = False
				else :
					logger.debug('Validation Pass: ' + content_id_column_heading + ' (' + content_id + ') is not having a new line character. Proceeding with further checks')

			if not isStartingWithLetters(content_id):
				logger.info('VALIDATION FAILED: ' + content_id_column_heading + ' (' + content_id + ') should be in correct format. Valid format is <CP_CODE><unique_id> where <CP_CODE> is unique two letters that defines CP and <unique_id> is number that defines the content uniquely for this CP. Example : DM00001 or AB54446')
				failureReasonForThisRow += content_id_column_heading + ' should be in correct format. Valid format is <CP_CODE><unique_id> where <CP_CODE> is unique two letters that defines CP and <unique_id> is number that defines the content uniquely for this CP. Example : DM00001 or AB54446'
				content_id_valid = False
			else:
				logger.debug('Validation Pass: ' + content_id_column_heading + ' (' + content_id + ') is in correct format. Valid format is <CP_CODE><unique_id> where <CP_CODE> is unique two letters that defines CP and <unique_id> is number that defines the content uniquely for this CP. Example : DM00001 or AB54446')

	else:
		if np.isnan(content_id):
			content_id = str(content_id)
			if ( content_id_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id_column_heading + ' can not be empty. It is mandatory field and must be unique')
				failureReasonForThisRow += content_id_column_heading + ' can not be empty. It is mandatory field and must be unique' + '|'
				content_id_valid = False
			else:
				logger.info('VALIDATION FAILED: ' + content_id_column_heading + ' (' + content_id + ') is mandatory field. Please update script config to add it in the list of mandatory fields')
				failureReasonForThisRow += content_id_column_heading + ' is mandatory field. Please update script config to add it in the list of mandatory fields'
				content_id_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + content_id_column_heading + ' (' + content_id + ') type is not correct')
			failureReasonForThisRow += content_id_column_heading + ' object data type is not correct'
			content_id_valid = False

		#failureReasonForThisRow += content_id_column_heading + ' can not be empty. It is mandatory field and must be unique'
		#return 'skip'

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))

	if content_id_valid :
		logger.debug('VALIDATION PASS: ' + content_id_column_heading + ' (' + content_id + ') is supplied correctly')
		return True;
	else :
		at_least_one_failure = True
		logger.debug('VALIDATION FAILED: ' + content_id_column_heading + ' (' + content_id + ') is not supplied correctly')
		return False


def validate_title ( title, title_column_heading ):
	global title_valid
	global at_least_one_failure
	global failureReasonForThisRow

	logger.debug(content_id + ': Validating ' + title_column_heading + ' (' + str(title) + ')')

	if isinstance(title, unicode):
		title       = str(title.strip())
		logger.debug( title_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		
		if ( title_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(title):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + title_column_heading + ' (' + title + ') is having a new line character which is not allowed')
				failureReasonForThisRow += title_column_heading + ' is having a new line character which is not allowed'  + '|'
				title_valid = False
			else :
				logger.debug('Validation Pass: ' + content_id + ': ' + title_column_heading + ' (' + title + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(title):
			if ( title_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': '+ title_column_heading + ' (' + str(title) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += title_column_heading + ' is mandatory field and must be supplied'  + '|'
				title_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + content_id + ': '+ title_column_heading + ') does not have correct object type')
			failureReasonForThisRow += title_column_heading + ' does not have correct object type' + '|'
			title_valid = False
	
	# Should be column no 2
	# If Type is Series/ TV Show, all episodes should have the same title 
	# if content_type in allowed_content_type:
		#if content_type in ('Series','TV Show'):
	# NOTE : This can not be verified
	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if title_valid :
		logger.debug(content_id + ':Validation Pass: ' + title_column_heading + ' (' + str(title) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + 'VALIDATION FAILED: ' + title_column_heading + ' (' + str(title) + ') is not supplied correctly')
		return False


def list_valid_content_type ( list ):
	str = ''
	for value in list:
		str = str + value + ','
	str = str[:-1]
	return str

def list_valid_genre ( list ):
	str = ''
	for value in list:
		str = str + value + ','
	str = str[:-1]
	return str

def validate_content_type ( local_content_type, local_content_type_column_heading): 
	global at_least_one_failure
	global content_type_valid
	global content_type
	global content_type_column_heading
	global failureReasonForThisRow
	content_type = local_content_type
	content_type_column_heading = local_content_type_column_heading

	if isinstance(content_type, unicode):
		content_type       = str(content_type.strip())
		# Debug statement 
		logger.debug( content_id + ': Validating ' + content_type_column_heading + ' (' + content_type + ')')

		# Should be only one of list mentioned in variable array - allowed_content_type
		if content_type.lower() not in ( contentType.lower() for contentType in allowed_content_type ):
		#if content_type not in allowed_content_type:
			logger.info( content_id + ': VALIDATION FAILED: ' + content_type_column_heading + ' (' + content_type + ') should be one of the following: ' + list_valid_content_type(allowed_content_type))
			failureReasonForThisRow += content_type_column_heading + ' should be one of the following: ' + list_valid_content_type(allowed_content_type) + '|'
			content_type_valid = False
		else:
			logger.debug(content_id + ': Validation Pass: ' + content_type_column_heading + ' (' + content_type + ') is one of the following: ' + list_valid_content_type(allowed_content_type) + '. Proceeding with further checks')

		# Should not have any special characters
		if isPresent_special_char_space_allowed(content_type):
			logger.info(content_id + ': VALIDATION FAILED: Found special characters in ' + content_type_column_heading + ' (' + content_type + ')')
			failureReasonForThisRow += content_type_column_heading + ' should not special characters' + '|'
			content_type_valid = False
		else:
			logger.debug(content_id + ': Validation Pass: ' + content_type_column_heading + ' (' + content_type + ') does not have any special character (' + special_chars_not_allowed + ')')
		
		logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	
	else:
		if np.isnan(content_type):
			if ( content_type_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': '+ content_type_column_heading + ' (' + str(content_type) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += content_type_column_heading + ' is mandatory field and must be supplied' + '|'
				content_type_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + content_id + ': '+ content_type_column_heading + ') does not have correct object type')
			failureReasonForThisRow += content_type_column_heading + ' does not have correct object type' + '|'
			content_type_valid = False

	if content_type_valid :
		logger.debug(content_id + ':Validation Pass: ' + content_type_column_heading + ' (' + str(content_type) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + 'VALIDATION FAILED: ' + content_type_column_heading + ' (' + str(content_type) + ') is not supplied correctly')
		return False

def validate_season_no ( season_no, season_no_column_heading) :
	global season_no_valid
	global at_least_one_failure
	global failureReasonForThisRow
	logger.debug(content_id + ': Validating ' + season_no_column_heading + ' (' + str(season_no) + ') for ' + content_type_column_heading + ' (' + str(content_type) + ')' )

	if isinstance(season_no, float) and not np.isnan(season_no) and content_type.lower() in (cat.lower() for cat in series_category):
		season_no = int(season_no)
		logger.debug( season_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		# validate season no
		
		# If movie, Season No & Episode No can be null

		# If "Series, Tv, Show, TvShow", Season No & Episode No can not be NULL
		if content_type.lower() in ( contentType.lower() for contentType in allowed_content_type ):
		#if content_type in allowed_content_type:

			if content_type.lower() in (cat.lower() for cat in series_category):
				if not season_no:
					logger.info(content_id + ': VALIDATION FAILED: For ' + content_type_column_heading + ' (' + content_type + '), ' + season_no_column_heading + ' can not be null')
					failureReasonForThisRow += season_no_column_heading + ' can not be null' + '|' 
					season_no_valid = False
				elif ( isinstance(season_no, str) ):
					logger.info(content_id + ': VALIDATION FAILED: For ' + content_type_column_heading + ' (' + content_type + ') ' + season_no_column_heading + ' is expected to be a number')
					failureReasonForThisRow += season_no_column_heading + ' is expected to be a number' + '|' 
					season_no_valid = False
		if ( season_no_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(season_no):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + season_no_column_heading + ' (' + str(season_no) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += season_no_column_heading + ' is having a new line character which is not allowed' + '|' 
				season_no_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + season_no_column_heading + ' (' + str(season_no) + ') is not having a new line character. Proceeding with further checks')
	elif isinstance(season_no, int) and not np.isnan(season_no) and content_type.lower() in (cat.lower() for cat in series_category):
		season_no = int(season_no)
		logger.debug( season_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		# validate season no
		
		# If movie, Season No & Episode No can be null

		# If "Series, Tv, Show, TvShow", Season No & Episode No can not be NULL
		if content_type.lower() in ( contentType.lower() for contentType in allowed_content_type ):
		#if content_type in allowed_content_type:
			if content_type.lower() in (cat.lower() for cat in series_category):
				if not season_no:
					logger.info(content_id + ': VALIDATION FAILED: For ' + content_type_column_heading + ' (' + content_type + ') ' + season_no_column_heading + ' can not be null')
					failureReasonForThisRow += season_no_column_heading + ' can not be null' + '|' 
					season_no_valid = False
				elif ( isinstance(season_no, str) ):
					logger.info(content_id + ': VALIDATION FAILED: For ' + content_type_column_heading + ' (' + content_type + ') ' + season_no_column_heading + ' is expected to be a number')
					failureReasonForThisRow += season_no_column_heading + ' is expected to be a number' + '|' 
					season_no_valid = False
		if ( season_no_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(season_no):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + season_no_column_heading + ' (' + str(season_no) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += season_no_column_heading + ' is having a new line character which is not allowed' + '|' 
				season_no_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + season_no_column_heading + ' (' + str(season_no) + ') is not having a new line character. Proceeding with further checks')

	elif( isinstance(season_no, unicode) and content_type.lower() in (cat.lower() for cat in series_category)):
		logger.info('VALIDATION FAILED: ' + content_id + ': For ' + content_type_column_heading + ' (' + content_type + '), ' + season_no_column_heading + ' is of type string. It should be a number')
		failureReasonForThisRow += season_no_column_heading + ' is of type string. It should be a number' + '|' 
		season_no_valid = False
	elif( not np.isnan(season_no) and content_type.lower() not in (cat.lower() for cat in series_category)):
		logger.info('VALIDATION FAILED: ' + content_id + ': For ' + content_type_column_heading + ' (' + content_type + '), ' + season_no_column_heading + ' should be null. Keep the feild blank')
		failureReasonForThisRow += season_no_column_heading + ' should be null. Keep the feild blank' + '|' 
		season_no_valid = False
	else:
		if np.isnan(season_no) and str(content_type).lower() in (cat.lower() for cat in series_category):
			logger.info('VALIDATION FAILED: For content type (' + content_type + '), ' + season_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is mandatory field and must be supplied')
			failureReasonForThisRow += season_no_column_heading + ' is mandatory field and must be supplied' + '|' 
			season_no_valid = False
		elif str(content_type).lower() in (cat.lower() for cat in series_category):
			logger.info('VALIDATION FAILED: ' + season_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += season_no_column_heading + ' does not have correct object type' + '|' 
			season_no_valid = False
		else:
			logger.debug(content_id + ': VALIDATION PASS: ' + season_no_column_heading + ' (' + str(season_no) + ') looks fine')

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if season_no_valid :
		logger.debug(content_id + ':Validation Pass: ' + season_no_column_heading + ' (' + str(season_no) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + 'VALIDATION FAILED: ' + season_no_column_heading + ' (' + str(season_no) + ') is not supplied correctly')
		failureReasonForThisRow += season_no_column_heading + ' is not supplied correctly' + '|' 
		return False

def validate_episode_no ( episode_no, episode_no_column_heading ) :
	global episode_no_valid
	global at_least_one_failure
	global failureReasonForThisRow
	# Debug statement 
	logger.debug(content_id + ': Validating ' + episode_no_column_heading + ' (' + str(episode_no) + ') for content_type : (' + str(content_type) + ')' )

	if isinstance(episode_no, float) and not np.isnan(episode_no) and content_type.lower() in (cat.lower() for cat in series_category):
		episode_no = int(episode_no)
		logger.debug( episode_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		# validate season no
		# If movie, Season No & Episode No can be null
		# If "Series, Tv, Show, TvShow", Season No & Episode No can not be NULL
		if content_type.lower() in ( contentType.lower() for contentType in allowed_content_type ):
		#if content_type in allowed_content_type:
			if content_type.lower() not in (cat.lower() for cat in series_category):
				if not episode_no:
					logger.info('VALIDATION FAILED: For content id (' + content_id + '), content type is (' + content_type + ') - '+ episode_no_column_heading +' can not be null')
					failureReasonForThisRow += episode_no_column_heading + '  can not be null' + '|' 
					episode_no_valid = False
				elif ( isinstance(episode_no, str) ):
					logger.info('VALIDATION FAILED: For content id (' + content_id + '), content type is (' + content_type + ') - '+ episode_no_column_heading +' is expected to be a number')
					failureReasonForThisRow += episode_no_column_heading + ' is expected to be a number' + '|' 
					episode_no_valid = False
		if ( episode_no_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(episode_no):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + episode_no_column_heading + ' (' + str(episode_no) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += episode_no_column_heading + ' is having a new line character which is not allowed' + '|' 
				episode_no_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + episode_no_column_heading + ' (' + str(episode_no) + ') is not having a new line character. Proceeding with further checks')
	elif isinstance(episode_no, int) and not np.isnan(episode_no) and content_type.lower() in (cat.lower() for cat in series_category):
		episode_no = int(episode_no)
		logger.debug( episode_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		# validate season no
		# If movie, Season No & Episode No can be null
		# If "Series, Tv, Show, TvShow", Season No & Episode No can not be NULL
		if content_type.lower() in ( contentType.lower() for contentType in allowed_content_type ):
		#if content_type in allowed_content_type:
			if content_type.lower() not in (cat.lower() for cat in series_category):
				if not episode_no:
					logger.info('VALIDATION FAILED: For content id (' + content_id + '), content type is (' + content_type + ') - '+ episode_no_column_heading +' can not be null')
					failureReasonForThisRow += episode_no_column_heading + ' can not be null' + '|' 
					episode_no_valid = False
				elif ( isinstance(episode_no, str) ):
					logger.info('VALIDATION FAILED: For content id (' + content_id + '), content type is (' + content_type + ') - '+ episode_no_column_heading +' is expected to be a number')
					failureReasonForThisRow += episode_no_column_heading + ' is expected to be a number' + '|' 
					episode_no_valid = False
		if ( episode_no_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(episode_no):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + episode_no_column_heading + ' (' + str(episode_no) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += episode_no_column_heading + ' is having a new line character which is not allowed' + '|' 
				episode_no_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + episode_no_column_heading + ' (' + str(episode_no) + ') is not having a new line character. Proceeding with further checks')
	elif( isinstance(episode_no, unicode) and content_type.lower() in (cat.lower() for cat in series_category)):
		logger.info('VALIDATION FAILED: For content type (' + content_type + '), ' + episode_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is of type string. It should be a number')
		failureReasonForThisRow += episode_no_column_heading + ' is of type string. It should be a number' + '|' 
		episode_no_valid = False
	elif( not np.isnan(episode_no) and content_type.lower() not in (cat.lower() for cat in series_category)):
		logger.info('VALIDATION FAILED: ' + content_id + ': For ' + content_type_column_heading + ' (' + content_type + '), ' + episode_no_column_heading + ' should be null. Keep the feild blank')
		failureReasonForThisRow += episode_no_column_heading + ' should be null. Keep the feild blank' + '|' 
		episode_no_valid = False
	else:
		if np.isnan(episode_no) and str(content_type).lower() in (cat.lower() for cat in series_category):
			logger.info('VALIDATION FAILED: For content type (' + content_type + '), ' + episode_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is mandatory field and must be supplied')
			failureReasonForThisRow += episode_no_column_heading + ' is mandatory field and must be supplied' + '|' 
			episode_no_valid = False
		elif str(content_type).lower() in (cat.lower() for cat in series_category):
			logger.info('VALIDATION FAILED: ' + episode_no_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += episode_no_column_heading + ' does not have correct object type' + '|' 
			episode_no_valid = False
		else:
			logger.debug(content_id + ': VALIDATION PASS: ' + episode_no_column_heading + ' (' + str(episode_no) + ') looks fine')

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if episode_no_valid :
		logger.debug(content_id + ':Validation Pass: ' + episode_no_column_heading + ' (' + str(episode_no) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + 'VALIDATION FAILED: ' + episode_no_column_heading + ' (' + str(episode_no) + ') is not supplied correctly')
		return False

def doubleQuotesBetweenString ( description ) :
	description_split_arr = description.split('"')
	if re.search ( '^".*"$', description ):
		if ( len(description_split_arr) > 3 ):
			return True
	else:
		if ( len(description_split_arr) > 1 ):
			return True
		else:
			return False

def validate_description ( description, description_column_heading ) :
	global at_least_one_failure
	global description_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug(content_id + ': Validating '+ description_column_heading +' (' + str(description) + ')')

	if isinstance(description, unicode):
		description = str(description.strip())
		logger.debug( description_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( description_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(description):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + description_column_heading + ' (' + str(description) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += description_column_heading + ' is having a new line character which is not allowed' + '|' 
				description_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + description_column_heading + ' (' + str(description) + ') is not having a new line character. Proceeding with further checks')
		if ( description_column_heading in double_quotes_not_supported_feilds ) :
			if ( doubleQuotesBetweenString(description) ):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + description_column_heading + ' (' + str(description) + ') contains double quotes in between the string. Allowed only at the beginning and end')
				failureReasonForThisRow += description_column_heading + ' contains double quotes in between the string. Allowed only at the beginning and end' + '|' 
				description_valid = False
			else:
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + description_column_heading + ' (' + str(description) + ') is not having a having double quotes in between the string')
			
	else:
		if np.isnan(description):
			description = str(description)
			if ( description_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + description_column_heading + ' (' + str(description) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += description_column_heading + ' is mandatory field and must be supplied' + '|' 
				description_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + description_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += description_column_heading + ' does not have correct object type' + '|' 
			description_valid = False
	
	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if description_valid :
		logger.debug(content_id + ':Validation Pass: ' + description_column_heading + ' (' + str(description) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + 'VALIDATION FAILED: ' + description_column_heading + ' (' + str(description) + ') is not supplied correctly')
		return False

def validate_artist ( artist, artist_column_heading ) :
	global at_least_one_failure
	global artist_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating artist (' + str(artist) + ') for content_id (' + content_id + ')')

	if isinstance(artist, unicode):
		artist = str(artist.strip())
		logger.debug( artist_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( artist_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(artist):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + artist_column_heading + ' (' + str(artist) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += artist_column_heading + ' is having a new line character which is not allowed' + '|' 
				artist_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + artist_column_heading + ' (' + str(artist) + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(artist):
			if ( artist_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + artist_column_heading + ' (' + str(artist) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += artist_column_heading + ' is mandatory field and must be supplied' + '|' 
				artist_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + artist_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += artist_column_heading + ' does not have correct object type' + '|' 
			artist_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if artist_valid :
		logger.debug('Validation Pass: artist (' + str(artist) + ') is supplied correctly for content id (' + content_id + ')')
		return True
	else :
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: artist (' + str(artist) + ') is not supplied correctly')
		return False

def validate_genre ( genre, genre_column_heading ) :
	global at_least_one_failure
	global genre_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating genre (' + str(genre) + ') for content_id (' + content_id + ')')
	if isinstance(genre, unicode):
		genre = str(genre.strip())
		logger.debug( genre_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if genre.lower() not in ( genList.lower() for genList in supported_genre_list):
			logger.info('VALIDATION FAILED: For content id (' + content_id + '), genre is (' + str(genre) + ') should be one of the following supported list (' + list_valid_genre(supported_genre_list) + ')')
			failureReasonForThisRow += genre_column_heading + ' should be one of the following supported list (' + list_valid_genre(supported_genre_list) + ')' + '|' 
			genre_valid = False

		if ( genre_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(genre):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + genre_column_heading + ' (' + str(genre) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += genre_column_heading + ' is having a new line character which is not allowed' + '|' 
				genre_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + genre_column_heading + ' (' + str(genre) + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(genre):
			if ( genre_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + genre_column_heading + ' (' + str(genre) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += genre_column_heading + ' is mandatory field and must be supplied' + '|' 
				genre_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + genre_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += genre_column_heading + ' does not have correct object type' + '|' 
			genre_valid = False
	
	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if genre_valid :
		logger.debug(content_id + ': VALIDATION PASS: genre (' + str(genre) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: genre (' + str(genre) + ') is not supplied correctly')
		return False



def validate_release_date ( release_date, release_date_column_heading ) :
	global at_least_one_failure
	global release_date_valid
	global failureReasonForThisRow
	logger.debug(content_id + ': Validating ' + release_date_column_heading + ' (' + str(release_date) + ')')
	# Should be a valid date format 
	if isinstance(release_date, unicode) or isinstance(release_date, int) and not np.isnan(release_date):
		logger.debug( str(release_date_column_heading) + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( isinstance(release_date, int) ):
			if ( release_date_oldest_content_year < release_date <= now.year):
				logger.debug(content_id + ': Validation Pass: The release date (' + str(release_date) +') is between (' + str(release_date_oldest_content_year) + ' to ' + str(now.year) + ')')
			else:
				logger.info(content_id + ': VALIDATION FAILED: The ' + release_date_column_heading + ' (' + str(release_date) +') should be in between (' + str(release_date_oldest_content_year) + ' to ' + str(now.year) + ')')
				failureReasonForThisRow += release_date_column_heading + ' should be in between (' + str(release_date_oldest_content_year) + ' to ' + str(now.year) + ')' + '|' 
				release_date_valid = False
			release_date = str(str(release_date).strip())
		else :
			release_date = str(str(release_date).strip())
		
		try:
			provided_release_date = parse(release_date, dayfirst=True)
			if parse(str(release_date_oldest_content_year) + "-01-01", dayfirst=True) < provided_release_date < now:
				logger.debug('Validation Pass: The release date (' + str(release_date) +') is supplied correctly for content id (' + content_id + ')')
			else:
				logger.info('VALIDATION FAILED: The ' + release_date_column_heading + ' (' + str(release_date) +') for ' + content_id_column_heading + ' (' + content_id + ') seems like either a future date or older than year (' + str(release_date_oldest_content_year) + ') . Today date is (' + str(now) + '). Make sure to provide date first and month followed by year')
				failureReasonForThisRow += release_date_column_heading + ' seems like either a future date or older than year (' + str(release_date_oldest_content_year) + ') . Today date is (' + str(now) + '). Make sure to provide date first and month followed by year' + '|' 
				release_date_valid = False
		except Exception as e:
			logger.debug("Exception: " + str(e))
			logger.info('VALIDATION FAILED: The ' + release_date_column_heading + 'e (' + str(release_date) +') is not in correct format for content id (' + content_id + ')')
			failureReasonForThisRow += release_date_column_heading + ' is not in correct format. Valid input - \'DD-MM-YYYY\' or \'DD/MM/YYYY\'' + '|' 
			release_date_valid = False

		if ( release_date_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(release_date):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + release_date_column_heading + ' (' + str(release_date) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += release_date_column_heading + ' is having a new line character which is not allowed' + '|' 
				release_date_valid = False
			else :
				logger.debug('Validation Pass: ' + content_id + ': ' + release_date_column_heading + ' (' + str(release_date) + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(release_date):
			if ( release_date_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + release_date_column_heading + ' (' + str(release_date) + ') is mandatory field and must be supplied. Valid input - \'DD-MM-YYYY\'')
				failureReasonForThisRow += release_date_column_heading + ' is mandatory field and must be supplied. Valid input - \'DD-MM-YYYY\'' + '|' 
				release_date_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + release_date_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type. Valid input - \'DD-MM-YYYY\' or \'DD/MM/YYYY\'')
			failureReasonForThisRow += release_date_column_heading + ' does not have correct object type. Valid input - \'DD-MM-YYYY\' or \'DD/MM/YYYY\'' + '|' 
			release_date_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if release_date_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + release_date_column_heading + ' (' + str(release_date) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + release_date_column_heading + ' (' + str(release_date) + ') is not supplied correctly')
		return False

def get_sec(time_str):
    """Get Seconds from time."""
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)

def validate_duration ( duration, duration_column_heading ) :
	global at_least_one_failure
	global duration_valid
	global failureReasonForThisRow
	# Should be a proper duration in format HH:MM:SS or MM:SS or SSS
	if isinstance(duration, unicode):
		duration = duration.strip()
		logger.debug( duration_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')

		if ( ':' in duration):
			if ( '"' in duration) :
				duration = duration.split('"')[1]
			logger.debug ( content_id + ': The ' + duration_column_heading + '(' + duration + ') does not seem to be provided in seconds')
			
			# validate duration
			try :
				datetime.datetime.strptime ( duration, "%H:%M:%S" )
				#validate_duration_length ( duration )
				logger.debug('Validation Pass: The duration (' + str(duration) +') is supplied correctly for content id (' + content_id + ')')
				if ( allowed_min_duration_in_seconds < get_sec(duration) < allowed_duration_in_seconds):
					logger.debug(content_id + ': Validation Pass: The duration (' + str(duration) +') is supplied correctly. It is within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours')
				else:
					logger.debug(content_id + ': VALIDATION FAILED: The ' + duration_column_heading + '  (' + str(duration) +') is more then expected. It should be within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours')
					failureReasonForThisRow += duration_column_heading + ' is more then expected. It should be within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours' + '|' 
					duration_valid = False
			except Exception as e:
				logger.debug('Error: ' + str(e))
				logger.debug('The duration (' + str(duration) +') is not in format HH:MM:SS for content id (' + content_id + '). Hence checking for format MM:SS')
				try:
					datetime.datetime.strptime ( str(duration), "%M:%S" )
					logger.debug('Validation Pass: The duration (' + str(duration) +') is supplied in format MM:SS correctly for content id (' + content_id + ')')

				except Exception as e:
					logger.debug('Error: ' + str(e))
					logger.info('VALIDATION FAILED: ' + content_id + ': ' + duration_column_heading + ' (' + str(duration)+ ') is not in correct datetime format')
					failureReasonForThisRow += duration_column_heading + ' is not in correct datetime format. It should be ( HH:MM:SS or MM:SS or seconds )' + '|' 
					duration_valid = False
		else:
			logger.debug ( content_id + ': The ' + duration_column_heading + ' seem to be provided in seconds')
			if ( '"' in duration) :
				logger.debug(content_id + ': Found double quotes in ' + duration_column_heading + '. Removing double quotes')
				duration = duration.split('"')[1]
				logger.debug(content_id + ': After removing double quotes in ' + duration_column_heading + ' the value is : ' + str(duration))
			try :
				duration = int(duration)
				if ( allowed_min_duration_in_seconds < duration < allowed_duration_in_seconds):
					logger.debug(content_id + ': Validation Pass: The duration (' + str(duration) +') is supplied correctly. It is within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours')
				else:
					logger.debug(content_id + ': VALIDATION FAILED: The ' + duration_column_heading + '  (' + str(duration) +') is more then expected. It should be within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours')
					failureReasonForThisRow += duration_column_heading + ' is more then expected. It should be within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours' + '|' 
					duration_valid = False
			except Exception as e:
				logger.debug('Error: ' + str(e))
				logger.debug(content_id + ': VALIDATION FAILED: The ' + duration_column_heading + '  (' + str(duration) +') is not supplied correctly')
				failureReasonForThisRow += duration_column_heading + ' is not supplied correctly' + '|' 
				duration_valid = False
		
		if ( duration_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(duration):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + duration_column_heading + ' (' + str(duration) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += duration_column_heading + ' is having a new line character which is not allowed' + '|' 
				duration_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + duration_column_heading + ' (' + str(duration) + ') is not having a new line character. Proceeding with further checks')
	elif( isinstance(duration, int)  or isinstance(duration, float) and not np.isnan(duration)):
		logger.debug('The duration is metioned in seconds. Validating if the total no of seconds is not exceeding ' + str(allowed_duration_in_seconds) )
		if ( allowed_min_duration_in_seconds < duration < allowed_duration_in_seconds):
			logger.debug(content_id + ': Validation Pass: The duration (' + str(duration) +') is supplied correctly. It is within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours')
		else:
			logger.debug(content_id + ': VALIDATION FAILED: The ' + duration_column_heading + '  (' + str(duration) +') is more then expected. It should be within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours')
			failureReasonForThisRow += duration_column_heading + ' is more then expected. It should be within range of (' + str(allowed_min_duration_in_seconds) + 'seconds and ' + str(allowed_duration_in_seconds/60/60) + ') hours' + '|' 
			duration_valid = False
	else:
		if np.isnan(duration):
			if ( duration_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + duration_column_heading + ' (' + str(duration) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += duration_column_heading + ' is mandatory field and must be supplied' + '|' 
				duration_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + duration_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += duration_column_heading + ' does not have correct object type' + '|' 
			duration_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if duration_valid:
		logger.debug(content_id + ': VALIDATION PASS: ' + duration_column_heading + ' (' + str(duration) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + duration_column_heading + ' (' + str(duration) + ') is not supplied correctly. It should be ( HH:MM:SS or MM:SS or seconds )')
		return False


def validate_duration_length ( duration ) :
	str(datetime.timedelta(seconds = duration))

def validate_language ( language, language_column_heading ) :
	global at_least_one_failure
	global language_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating language (' + str(language) + ') for content_id (' + content_id + ')')

	if isinstance(language, unicode):
		language = str(language.strip())
		logger.debug( language_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		
		if ( language_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(language):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + language_column_heading + ' (' + language + ') is having a new line character which is not allowed')
				failureReasonForThisRow += language_column_heading + ' is having a new line character which is not allowed' + '|' 
				language_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + language_column_heading + ' (' + language + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(language):
			if ( language_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + language_column_heading + ' (' + str(language) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += language_column_heading + ' is mandatory field and must be supplied' + '|' 
				language_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + language_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += language_column_heading + ' does not have correct object type' + '|' 
			language_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if language_valid :
		logger.debug('VALIDATION PASS: language (' + str(language) + ') is supplied correctly for content id (' + content_id + ')')
		return True
	else :
		at_least_one_failure = True
		logger.debug('VALIDATION FAILED: language (' + str(language) + ') is not supplied correctly for content id (' + content_id + ')')
		return False
	

def validate_content_producer ( content_producer, content_producer_column_heading ) :
	global at_least_one_failure
	global content_producer_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating content producer (' + str(content_producer) + ') for content_id (' + content_id + ')')

	if isinstance(content_producer, unicode):
		content_producer = str(content_producer.strip())
		logger.debug( content_producer_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		
		if ( content_producer_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(content_producer):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + content_producer_column_heading + ' (' + content_producer + ') is having a new line character which is not allowed')
				failureReasonForThisRow += content_producer_column_heading + ' is having a new line character which is not allowed' + '|' 
				content_producer_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + content_producer_column_heading + ' (' + content_producer + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(content_producer):
			if ( content_producer_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + content_producer_column_heading + ' (' + str(content_producer) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += content_producer_column_heading + ' is mandatory field and must be supplied' + '|' 
				content_producer_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + content_producer_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += content_producer_column_heading + ' does not have correct object type' + '|' 
			content_producer_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if content_producer_valid :
		logger.debug(content_id + ': VALIDATION PASS: content producer (' + str(content_producer) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: content producer (' + str(content_producer) + ') is not supplied correctly')
		return False

def list_valid_parental_guide ( list ):
	str = ''
	for value in list:
		str = str + value + '/'
	str = str[:-1]
	return str

def validate_parental_guide ( parental_guide, parental_guide_column_heading ) :
	global at_least_one_failure
	global parental_guide_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug(content_id + ': Validating ' + parental_guide_column_heading + ' (' + str(parental_guide) + ')')

	parental_guide_column_heading = content.columns[12]
	if isinstance(parental_guide, unicode) or isinstance(parental_guide, int) and not np.isnan(parental_guide):
		parental_guide = str(str(parental_guide).strip())
		logger.debug( parental_guide_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')

		if parental_guide.lower() not in ( pg.lower() for pg in parental_guide_allowed_values):
			logger.info('VALIDATION FAILED: parental guide (' + parental_guide + ') for content id (' + content_id + ') should be one of ' + list_valid_parental_guide(parental_guide_allowed_values))
			failureReasonForThisRow += parental_guide_column_heading + ' should be one of ' + list_valid_parental_guide(parental_guide_allowed_values) + '|'
			parental_guide_valid = False

		if ( parental_guide_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(parental_guide):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + parental_guide_column_heading + ' (' + parental_guide + ') is having a new line character which is not allowed')
				failureReasonForThisRow += parental_guide_column_heading + ' is having a new line character which is not allowed' + '|'
				parental_guide_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + parental_guide_column_heading + ' (' + parental_guide + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(parental_guide):
			if ( parental_guide_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + parental_guide_column_heading + ' (' + str(parental_guide) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += parental_guide_column_heading + ' is mandatory field and must be supplied' + '|'
				parental_guide_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + parental_guide_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')		
			failureReasonForThisRow += parental_guide_column_heading + ' does not have correct object type' + '|'
			parental_guide_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if parental_guide_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + parental_guide_column_heading + ' (' + parental_guide + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + parental_guide_column_heading + ' (' + parental_guide + ') is not supplied correctly')
		return False

def isURL ( resource_file ) :
	try:
		result = urlparse(resource_file)
		if ( not result.scheme or not result.netloc or not result.path ):
			return 0	
		else:
			return 1
		return all([result.scheme, result.netloc, result.path])
	except:
		logger.debug('ERROR: URL parser module is not able to parse the resource file (' + resource_file + ')')
		return False

def list_supported_movie_formats ( list ):
	str = ''
	for value in list:
		str = str + value + ','
	str = str[:-1]
	return str

def is_url_valid ( url ) :
	try:
		request = requests.get(url)
		if request.status_code == 200:
			return 1
		else :
			return 0
	except:
		return 0

def does_name_contains_content_id ( name, content_id, resource_file_column_heading):
	logger.debug('Filname:' + str(name) + ', Content Id: ' + str(content_id) + ', Column: ' + str(resource_file_column_heading))
	if ( resource_file_column_heading == 'ResourceFile'):
		#if ( re.match('\\b'+content_id+'\\b', name) or re.match('\\b'+content_id+'_play\\b', name) ):
		if ( re.match('\\b'+content_id+'_play\\b', name, re.IGNORECASE) or re.match('\\b'+content_id+'\\b', name, re.IGNORECASE)):
			return 1
		else:
			return 0
	if ( resource_file_column_heading == 'Trailer'):
		# if ( re.match('\\b'+content_id+'\\b', name) or re.match('\\b'+content_id+'_trailer\\b', name) ):
		if ( re.match('\\b'+content_id+'_trailer\\b', name, re.IGNORECASE) ):
			return 1
		else:
			return 0

def validate_file_presense_in_s3 ( resource_file ):
	logger.debug('Validating resource file (' + resource_file + ') in present in S3')
	## INITIALIZE BOTO CONNECTION TO AWS S3
	session = boto3.Session( aws_access_key_id=access_key, aws_secret_access_key=secret_key )
	s3 = session.resource('s3')

	try:
	    s3.Object(destination_bucket, cp_home_directory+'/'+resource_file).load()
	except botocore.exceptions.ClientError as e:
	    if e.response['Error']['Code'] == "404":
	        # The object does not exist.
	        logger.debug('The key  (' + cp_home_directory+'/'+resource_file + ') is not present in Bucket (' + destination_bucket + ')')
	        return False
	    else:
	        # Something else has gone wrong.
	        logger.debug('Exception:' + str(e))
	        return False
	else:
	    # The object does exist.
	    logger.debug('The key  (' + cp_home_directory+'/'+resource_file + ') is present in Bucket (' + destination_bucket + ')')
	    return True

def validate_resource_file ( resource_file, resource_file_column_heading ) :
	global at_least_one_failure
	global resource_file_valid
	global failureReasonForThisRow

	logger.debug(content_id + ': Validating ' + resource_file_column_heading + ' (' + str(resource_file) + ')')
	if isinstance(resource_file, unicode):
		resource_file = str(resource_file.strip())
		logger.debug( resource_file_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( resource_file_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(resource_file):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + resource_file_column_heading + ' (' + resource_file + ') is having a new line character which is not allowed')
				failureReasonForThisRow += resource_file_column_heading + ' is having a new line character which is not allowed' + '|'
				resource_file_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + resource_file_column_heading + ' (' + resource_file + ') is not having a new line character. Proceeding with further checks')
		
		if not isURL ( resource_file ) :
			logger.debug('The ' + resource_file_column_heading + '(' + resource_file +') for content id (' + content_id + ') does not seem to be a valid URL. So assuming it is avaible in the local file system. Checking for valid extension')
			# Not a URL, so it should be a mp4 file extension
			resource_file_extension = resource_file.split ('.')[-1]
			resource_file_name      = resource_file.split ('.')[0]
			#logger.debug('For content id (' + content_id + '), the resource file name is (' + resource_file_name + ') and its extension is (' + resource_file_extension +')')
			logger.debug('For content id (' + content_id + '), checking if ' + resource_file_column_heading + ' extension (' + resource_file_extension + ') is a valid extension')
			
			if ( resource_file_extension.lower() not in (format.lower() for format in supported_movie_formats)):
				logger.info('VALIDATION FAILED: The ' + resource_file_column_heading + ' extension for content id (' + content_id + ') is ' + resource_file_extension + '. Required is : ' + list_supported_movie_formats(supported_movie_formats) + '. The valid format is <contentID_play.mp4> or <contentID.mp4> or URL' )
				failureReasonForThisRow += resource_file_column_heading + ' extension is not correct. The valid format is <contentID_play.mp4> or <contentID.mp4>  or URL' + '|'
				resource_file_valid = False
			else:
				logger.debug('Validation Pass: The ' + resource_file_column_heading + ' extension for content id (' + content_id + ') is ' + resource_file_extension + ' and is a valid extension')
				logger.debug('For content id (' + content_id + '), checking if ' + resource_file_column_heading + ' name (' + resource_file_name + ') is a valid filename')
				# Should have Content ID present like : _contentId_<filename>.mp4
				if not does_name_contains_content_id ( resource_file_name, content_id, resource_file_column_heading):
					resource_file_valid = False
					logger.info('VALIDATION FAILED: The ' + resource_file_column_heading + ' (' + resource_file + ') for content id (' + content_id + ') does not have correct format. The valid format is <contentID_play.mp4> or <contentID.mp4> or URL' )
					failureReasonForThisRow += resource_file_column_heading + ' format is not correct. The valid format is <contentID_play.mp4> or <contentID.mp4> or URL' + '|'
				else:
					logger.debug('Validation Pass: The ' + resource_file_column_heading + ' name for content id (' + content_id + ') is ' + resource_file_name + ' and is a valid filename')
					# Filename & extension looks valid, validate if file is present in the present directory
					if ( files_location == 's3'):
						logger.debug('The files seems to be in (' + files_location + '). Validating if file is present in (' + files_location + ') by making boto3 call')
						if ( validate_file_presense_in_s3(resource_file) ):
							logger.debug( content_id + ': Validation Pass: The ' + resource_file_column_heading + ' (' + resource_file + ') for content id (' + content_id + ') is present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+resource_file + ')')
						else:
							logger.info( content_id + ': VALIDATION FAILED: The ' + resource_file_column_heading + ' (' + resource_file + ') for content id (' + content_id + ') is not present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+resource_file + ')')
							failureReasonForThisRow += resource_file_column_heading + ' is not present in the S3 location' + '|'
							resource_file_valid = False
					else :
						if path.exists(resource_file):
							logger.debug('The ' + resource_file_column_heading + ' (' + resource_file + ') for content id (' + content_id + ') is present in the current directory.')
						else:
							resource_file_valid = False
							logger.info('VALIDATION FAILED: The ' + resource_file_column_heading + ' (' + resource_file + ') for content id (' + content_id + ') is not present in the current directory (' + working_dir + ')')
							failureReasonForThisRow += resource_file_column_heading + ' is not present in the current directory' + '|'
		else:
			logger.debug('The ' + resource_file_column_heading + ' (' + resource_file +') for content id (' + content_id + ') is a valid URL format. Checking if URL is reachable')
			# Should be a valid URL if served from CP's cloudfront
			if not is_url_valid ( resource_file ) :
				resource_file_valid = False
				logger.info('VALIDATION FAILED: The ' + resource_file_column_heading + ' URL ' + resource_file + ' is not reachable')
				failureReasonForThisRow += resource_file_column_heading + ' is not reachable' + '|'
			else:
				logger.debug('Validation Pass: The ' + resource_file_column_heading + ' URL (' + resource_file +') for content id (' + content_id + ') is reachable')
	else:
		if np.isnan(resource_file):
			if ( resource_file_column_heading in columns_requiring_validation ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + resource_file_column_heading + ' (' + str(resource_file) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += resource_file_column_heading + ' is mandatory field and must be supplied' + '|'
				resource_file_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + resource_file_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')		
			failureReasonForThisRow += resource_file_column_heading + ' does not have correct object type' + '|'
	
	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))

	if resource_file_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + resource_file_column_heading + ' (' + str(resource_file) + ') is supplied correctly')
		array_of_successsfully_validated_resources.append(resource_file)
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + resource_file_column_heading + ' (' + str(resource_file) + ') is not supplied correctly or file is missing from the disk')
		return False

def list_subtitles_allowed_countries ( list ):
	str = ''
	for value in list:
		str = str + value + ','
	str = str[:-1]
	return str

def validate_subtitle ( subtitles, subtitles_column_heading ):
	global at_least_one_failure
	global subtitle_valid
	global failureReasonForThisRow
	logger.debug(content_id + ': Validating ' + subtitles_column_heading + ' (' + str(subtitles) + '). Flag is : ' + str(subtitle_valid))

	if isinstance(subtitles, unicode):
		subtitles = str(subtitles.strip())
		logger.debug( subtitles_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		for subtitle in subtitles.split('|'):
			subtitle = subtitle.strip()
			subtitle = str(subtitle)
			subtitle_have_valid_country = False
			subtitle_have_content_id = False
			subtitle_have_valid_format = False
			srt_name_without_extension = subtitle.split('.srt')[0]
			srt_country = srt_name_without_extension.split('_')[-1]
			srt_name_without_country_and_extension = srt_name_without_extension.split('_')[0]
			if ( re.match ('\\b'+content_id+'\\b', srt_name_without_country_and_extension, re.IGNORECASE ) ):
				logger.debug( "Validation Pass: " + content_id + ': The ' + subtitles_column_heading + ' (' + subtitle + ') contains ' + content_id_column_heading )
				subtitle_have_content_id = True
			else:
				logger.debug( "VALIDATION FAILED: " + content_id + ': The ' + subtitles_column_heading + ' (' + subtitle + ') does not contain ' + content_id_column_heading + '. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')')
				failureReasonForThisRow += subtitles_column_heading + ' does not contain '  + content_id_column_heading + '. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')' + '|'
				subtitle_have_content_id = False

			if srt_country.lower() in (qr.lower() for qr in subtitles_allowed_countries):
			#if srt_country in subtitles_allowed_countries :
				logger.debug( "Validation Pass: " + content_id + ': The ' + subtitles_column_heading + ' (' + subtitle + ') contains ' + srt_country + ' which is in the allowed list of srt countries (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')')
				subtitle_have_valid_country = True
			else :
				logger.debug( "VALIDATION FAILED: " + content_id + ': The ' + subtitles_column_heading + ' (' + subtitle + ') does not contain valid country code. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')')
				failureReasonForThisRow += subtitles_column_heading + ' does not contain valid country code. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')' + '|'
				subtitle_have_valid_country = False

			if ( re.match(r"(.+?)\_..\.srt", subtitle, re.IGNORECASE ) ):
				logger.debug( "Validation Pass: " + content_id + ': The subtitle  naming covention for ' + subtitles_column_heading + ' (' + subtitle + ') seems correct')
				subtitle_have_valid_format = True
			else:
				logger.debug( "VALIDATION FAILED: " + content_id + ': The subtitle  naming covention for ' + subtitles_column_heading + ' (' + subtitle + ') seems not correct. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')')
				failureReasonForThisRow += subtitles_column_heading + ' naming covention seems not correct. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')' + '|'
				subtitle_have_valid_format = False

			if ( subtitle_have_valid_country and subtitle_have_content_id and subtitle_have_valid_format ) :
				#subtitle_valid = True
				logger.debug( "Validation Pass: " + content_id + ': The subtitle  naming covention for ' + subtitles_column_heading + ' (' + subtitle + ') seems correct. ')
				if ( files_location == 's3'):
					logger.debug('The ' + subtitles_column_heading + ' ( ' + subtitle + ' ) seems to be in (' + files_location + '). Validating if it is present in (' + files_location + ') by making boto3 call')
					if ( validate_file_presense_in_s3(subtitle) ):
						logger.debug( content_id + ': Validation Pass: The ' + subtitles_column_heading + ' (' + subtitle + ') is present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+subtitle + ')')
					else:
						logger.info( content_id + ': VALIDATION FAILED: The ' + subtitles_column_heading + ' (' + subtitle + ') is not present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+subtitle + ')')
						failureReasonForThisRow += subtitles_column_heading + ' is not present in the S3 location' + '|'
						subtitle_valid = False
				else :
					if path.exists(subtitle):
						logger.debug(content_id + ': The ' + subtitles_column_heading + ' (' + subtitle + ') is present in the current directory (' + working_dir + ')')
					else:
						resource_file_valid = False
						logger.info(content_id + ': VALIDATION FAILED: The ' + subtitles_column_heading + ' (' + subtitle + ') is not present in the current directory (' + working_dir + ')')
						failureReasonForThisRow += subtitles_column_heading + ' is not present in the current directory' + '|'
			else:
				logger.info( "VALIDATION FAILED: " + content_id + ': The naming covention for ' + subtitles_column_heading + ' (' + subtitle + ') seems not correct. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')')
				failureReasonForThisRow += subtitles_column_heading + ' naming covention seems not correct. The correct format is : ' + content_id_column_heading + '_<country_code>.srt where country_code is one of (' + list_subtitles_allowed_countries(subtitles_allowed_countries) + ')' + '|'
				subtitle_valid = False

	else:
		if np.isnan(subtitles):
			if ( subtitles_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + subtitles_column_heading + ' (' + str(subtitles) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += subtitles_column_heading + ' is mandatory field and must be supplied' + '|'
				subtitle_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + content_id + ': ' + subtitles_column_heading + ' does not have correct object type')		
			failureReasonForThisRow += subtitles_column_heading + ' does not have correct object type' + '|'
			subtitle_valid = False
	
	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if subtitle_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + subtitles_column_heading + ' (' + str(subtitles) + ') is supplied correctly')
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + subtitles_column_heading + ' (' + str(subtitles) + ') is not supplied correctly')
		return False
			
def validate_trailer ( trailer, trailer_column_heading ) :
	global at_least_one_failure
	global trailer_valid
	global failureReasonForThisRow

	logger.debug(content_id + ': Validating ' + trailer_column_heading + ' (' + str(trailer) + ')')
	if isinstance(trailer, unicode):
		trailer = str(trailer.strip())
		logger.debug( trailer_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( trailer_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(trailer):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + trailer_column_heading + ' (' + trailer + ') is having a new line character which is not allowed')
				failureReasonForThisRow += trailer_column_heading + ' is having a new line character which is not allowed' + '|'
				trailer_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + trailer_column_heading + ' (' + trailer + ') is not having a new line character. Proceeding with further checks')
		
		if not isURL ( trailer ) :
			logger.debug('The ' + trailer_column_heading + '(' + trailer +') for content id (' + content_id + ') does not seem to be a valid URL. So assuming it is avaible in the local file system. Checking for valid extension')
			# Not a URL, so it should be a mp4 file extension
			trailer_extension = trailer.split ('.')[-1]
			trailer_name      = trailer.split ('.')[0]
			#logger.debug('For content id (' + content_id + '), the resource file name is (' + trailer_name + ') and its extension is (' + trailer_extension +')')
			logger.debug('For content id (' + content_id + '), checking if ' + trailer_column_heading + ' extension (' + trailer_extension + ') is a valid extension')
			
			if ( trailer_extension.lower() not in (format.lower() for format in supported_movie_formats)):
				logger.info('VALIDATION FAILED: The ' + trailer_column_heading + ' extension for content id (' + content_id + ') is ' + trailer_extension + '. Required is : ' + list_supported_movie_formats(supported_movie_formats) + '. The valid format is <contentID_trailer.mp4> or URL' )
				failureReasonForThisRow += trailer_column_heading + ' extension is not correct. The valid format is <contentID_trailer.mp4> or URL' + '|'
				trailer_valid = False
			else:
				logger.debug('Validation Pass: The ' + trailer_column_heading + ' extension for content id (' + content_id + ') is ' + trailer_extension + ' and is a valid extension')
				logger.debug('For content id (' + content_id + '), checking if ' + trailer_column_heading + ' name (' + trailer_name + ') is a valid filename')
				# Should have Content ID present like : _contentId_<filename>.mp4
				if not does_name_contains_content_id ( trailer_name, content_id, trailer_column_heading):
					trailer_valid = False
					logger.info('VALIDATION FAILED: The ' + trailer_column_heading + ' (' + trailer + ') for content id (' + content_id + ') does not have correct format. The valid format is <contentID_trailer.mp4> or URL' )
					failureReasonForThisRow += trailer_column_heading + ' format is not correct. The valid format is <contentID_trailer.mp4> or URL' + '|'
				else:
					logger.debug('Validation Pass: The ' + trailer_column_heading + ' name for content id (' + content_id + ') is ' + trailer_name + ' and is a valid filename')
					# Filename & extension looks valid, validate if file is present in the present directory
					if ( files_location == 's3'):
						logger.debug('The files seems to be in (' + files_location + '). Validating if file is present in (' + files_location + ') by making boto3 call')
						if ( validate_file_presense_in_s3(trailer) ):
							logger.debug( content_id + ': Validation Pass: The ' + trailer_column_heading + ' (' + trailer + ') for content id (' + content_id + ') is present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+trailer + ')')
						else:
							logger.info( content_id + ': VALIDATION FAILED: The ' + trailer_column_heading + ' (' + trailer + ') for content id (' + content_id + ') is not present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+trailer + ')')
							failureReasonForThisRow += trailer_column_heading + ' is not present in the S3 location' + '|'
							trailer_valid = False
					else :
						if path.exists(trailer):
							logger.debug('The ' + trailer_column_heading + ' (' + trailer + ') for content id (' + content_id + ') is present in the current directory.')
						else:
							trailer_valid = False
							logger.info('VALIDATION FAILED: The ' + trailer_column_heading + ' (' + trailer + ') for content id (' + content_id + ') is not present in the current directory (' + working_dir + ')')
							failureReasonForThisRow += trailer_column_heading + ' is not present in the current directory' + '|'
		else:
			logger.debug('The ' + trailer_column_heading + ' (' + trailer +') for content id (' + content_id + ') is a valid URL format. Checking if URL is reachable')
			# Should be a valid URL if served from CP's cloudfront
			if not is_url_valid ( trailer ) :
				trailer_valid = False
				logger.info('VALIDATION FAILED: The ' + trailer_column_heading + ' URL ' + trailer + ' is not reachable')
				failureReasonForThisRow += trailer_column_heading + ' is not reachable' + '|'
			else:
				logger.debug('Validation Pass: The ' + trailer_column_heading + ' URL (' + trailer +') for content id (' + content_id + ') is reachable')
	else:
		if np.isnan(trailer):
			if ( trailer_column_heading in columns_requiring_validation ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + trailer_column_heading + ' (' + str(trailer) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += trailer_column_heading + ' is mandatory field and must be supplied' + '|'
				trailer_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + trailer_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')		
			failureReasonForThisRow += trailer_column_heading + ' does not have correct object type' + '|'
			trailer_valid = False
	
	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))

	if trailer_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + trailer_column_heading + ' (' + str(trailer) + ') is supplied correctly')
		array_of_successsfully_validated_resources.append(trailer)
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + trailer_column_heading + ' (' + str(trailer) + ') is not supplied correctly or file is missing from the disk')
		return False
	

def does_image_contains_content_id ( name, content_id, image_file_expected_text):
	if ( re.match ('\\b'+content_id+image_file_expected_text+'\\b', name ), re.IGNORECASE ) :
		return 1
	else :
		return 0 
			
def validate_image_file ( image_file, image_file_heading, image_file_expected_text ) :
	global failureReasonForThisRow
	return_value = True
	if not isURL ( image_file ) :
		logger.debug('The ' + image_file_heading + '(' + image_file +') for content id (' + content_id + ') does not seem to be a valid URL. So assuming it is avaible in the local file system. Checking for valid extension')
		
		# Should not have any special characters
		if isPresent_special_char(image_file):
			logger.info('VALIDATION FAILED: Found either spaces or special characters in ' + image_file_heading + ' (' + image_file + '). It should not contain spaces or any of (' + special_chars_not_allowed + ')')
			failureReasonForThisRow += image_file_heading + ' contains either spaces or special characters.' + '|'
			return_value = False
		else:
			logger.debug('Validation Pass: The ' + image_file_heading + ' (' + image_file + ') does not have any spaces or special character (' + special_chars_not_allowed + ')')

		image_file_extension = image_file.split ('.')[-1]
		image_file_name      = image_file.split ('.')[0]

		logger.debug('For content id (' + content_id + '), checking if ' + image_file_heading + ' extension (' + image_file_extension + ') is a valid extension')
		if ( image_file_extension.lower() not in ( image.lower() for image in supported_image_formats)):
			logger.info('VALIDATION FAILED: The ' + image_file_heading + ' extension for content id (' + content_id + ') is ' + image_file_extension + '. Required is : ' + list_supported_image_formats(supported_image_formats) )
			failureReasonForThisRow += image_file_heading + ' does not have correct extension. Required is : ' + list_supported_image_formats(supported_image_formats) + '|'
			return_value = False
		else:
			logger.debug('Validation Pass: The ' + image_file_heading + ' extension for content id (' + content_id + ') is ' + image_file_extension + ' and is a valid extension')
			logger.debug('For content id (' + content_id + '), checking if ' + image_file_heading + ' name (' + image_file_name + ') is a valid filename')
			# Should have Content ID present like : _contentId_<filename>.mp4
			if not does_image_contains_content_id ( image_file_name, content_id, image_file_expected_text):
				logger.info('VALIDATION FAILED: The ' + image_file_heading + ' (' + image_file + ') for content id (' + content_id + ') does not have content id (' + content_id + ') preset in the resource file name' )
				failureReasonForThisRow += image_file_heading + ' does not have ' + content_id_column_heading + ' preset in the name' + '|'
				return_value = False
			else:
				logger.debug('Validation Pass: The ' + image_file_heading + ' name for content id (' + content_id + ') is ' + image_file_name + ' and is a valid filename')
				if ( files_location == 's3'):
					logger.debug('The files seems to be in (' + files_location + '). Validating if file is present in (' + files_location + ') by making boto3 call')
					if ( validate_file_presense_in_s3(image_file) ):
						logger.debug( content_id + ': Validation Pass: The ' + image_file_heading + ' (' + image_file + ') for content id (' + content_id + ') is present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+image_file + ')')
					else:
						logger.info( content_id + ': VALIDATION FAILED: The ' + image_file_heading + ' (' + image_file + ') for content id (' + content_id + ') is not present in the S3 location (' + destination_bucket + '/' + cp_home_directory+'/'+image_file + ')')
						failureReasonForThisRow += image_file_heading + ' is not present in the S3 location' + '|'
						return_value = False
				elif (files_location == 'local'):
					# Filename & extension looks valid, validate if file is present in the present directory
					if path.exists(image_file):
						logger.debug('The ' + image_file_heading + ' (' + image_file + ') for content id (' + content_id + ') is present in the current directory.')
					else:
						logger.info('VALIDATION FAILED: The ' + image_file_heading + ' (' + image_file + ') for content id (' + content_id + ') is not present in the current directory (' + working_dir + ')')
						failureReasonForThisRow += image_file_heading + ' is not present in current working directory' + '|'
						return_value = False
	else:
		logger.debug('The ' + image_file_heading + ' (' + image_file +') for content id (' + content_id + ') is a valid URL format. Checking if URL is reachable')
		# Should be a valid URL if served from CP's cloudfront
		if not is_url_valid ( image_file ) :
			logger.info('VALIDATION FAILED: The ' + image_file_heading + ' URL ' + image_file + ' is not reachable')
			failureReasonForThisRow += image_file_heading + ' is not reachable' + '|'
			return_value = False
		else:
			logger.debug('Validation Pass: The ' + image_file_heading + ' URL (' + image_file +') for content id (' + content_id + ') is reachable')

	if return_value :
		logger.debug(content_id+ ': Image validations seems fine. Return True for the function call')
		return True
	else:
		logger.debug(content_id+ ': Image validations does not seems fine. Return False for the function call')
		return False

def validate_landscape_4x3 ( landscape_4x3, landscape_4x3_column_heading ):
	global landscape_4x3_valid
	global at_least_one_failure
	global failureReasonForThisRow

	logger.debug(content_id + ': Validating ' + landscape_4x3_column_heading + ' (' + str(landscape_4x3) + ')')
	
	if isinstance(landscape_4x3, unicode):
		landscape_4x3 = str(landscape_4x3.strip())
		logger.debug( landscape_4x3_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( landscape_4x3_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(landscape_4x3):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + landscape_4x3_column_heading + ' (' + landscape_4x3 + ') is having a new line character which is not allowed')
				failureReasonForThisRow += landscape_4x3_column_heading + ' is having a new line character which is not allowed' + '|'
				landscape_4x3_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + landscape_4x3_column_heading + ' (' + landscape_4x3 + ') is not having a new line character. Proceeding with further checks')
		# validate landscape_4x3
		landscape_4x3_valid = validate_image_file ( landscape_4x3, landscape_4x3_column_heading, '_4x3' )
	else:
		if np.isnan(landscape_4x3):
			if ( landscape_4x3_column_heading in columns_requiring_validation ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + landscape_4x3_column_heading + ' (' + str(landscape_4x3) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += landscape_4x3_column_heading + ' is mandatory field and must be supplied' + '|'
				landscape_4x3_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + landscape_4x3_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += landscape_4x3_column_heading + ' does not have correct object type' + '|'
			landscape_4x3_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if landscape_4x3_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + landscape_4x3_column_heading + ' (' + str(landscape_4x3) + ') is supplied correctly')
		array_of_successsfully_validated_resources.append(landscape_4x3)
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + landscape_4x3_column_heading + ' (' + str(landscape_4x3) + ') is not supplied correctly')
		return False
		

def validate_portrait_2x3 ( portrait_2x3, portrait_2x3_column_heading ):
	global portrait_2x3_valid
	global at_least_one_failure
	global failureReasonForThisRow
	logger.debug(content_id + ': Validating ' + portrait_2x3_column_heading + ' (' + str(portrait_2x3) + ')')
	
	if isinstance(portrait_2x3, unicode):
		portrait_2x3 = str(portrait_2x3.strip())
		logger.debug( portrait_2x3_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( portrait_2x3_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(portrait_2x3):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + portrait_2x3_column_heading + ' (' + portrait_2x3 + ') is having a new line character which is not allowed')
				failureReasonForThisRow += portrait_2x3_column_heading + ' is having a new line character which is not allowed' + '|'
				portrait_2x3_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + portrait_2x3_column_heading + ' (' + portrait_2x3 + ') is not having a new line character. Proceeding with further checks')
		# validate portrait_2x3
		portrait_2x3_valid = validate_image_file ( portrait_2x3, portrait_2x3_column_heading, '_2x3' )
	else:
		if np.isnan(portrait_2x3):
			if ( portrait_2x3_column_heading in columns_requiring_validation ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + portrait_2x3_column_heading + ' (' + str(portrait_2x3) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += portrait_2x3_column_heading + ' is mandatory field and must be supplied' + '|'
				portrait_2x3_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + portrait_2x3_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += portrait_2x3_column_heading + ' does not have correct object type' + '|'
			portrait_2x3_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if portrait_2x3_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + portrait_2x3_column_heading + ' (' + str(portrait_2x3) + ') is supplied correctly')
		array_of_successsfully_validated_resources.append(portrait_2x3)
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + portrait_2x3_column_heading + ' (' + str(portrait_2x3) + ') is not supplied correctly')
		return False

def validate_landscape_16x9 ( landscape_16x9, landscape_16x9_column_heading ):
	global landscape_16x9_valid
	global at_least_one_failure
	global failureReasonForThisRow
	logger.debug(content_id + ': Validating ' + landscape_16x9_column_heading + ' (' + str(landscape_16x9) + ')')
	
	if isinstance(landscape_16x9, unicode):
		landscape_16x9 = str(landscape_16x9.strip())
		logger.debug( landscape_16x9_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( landscape_16x9_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(landscape_16x9):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + landscape_16x9_column_heading + ' (' + landscape_16x9 + ') is having a new line character which is not allowed')
				failureReasonForThisRow += landscape_16x9_column_heading + ' is having a new line character which is not allowed' + '|'
				landscape_16x9_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + landscape_16x9_column_heading + ' (' + landscape_16x9 + ') is not having a new line character. Proceeding with further checks')
		# validate landscape_16x9
		landscape_16x9_valid = validate_image_file ( landscape_16x9, landscape_16x9_column_heading, '_16x9' )
	else:
		if np.isnan(landscape_16x9):
			if ( landscape_16x9_column_heading in columns_requiring_validation ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + landscape_16x9_column_heading + ' (' + str(landscape_16x9) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += landscape_16x9_column_heading + ' is mandatory field and must be supplied' + '|'
				landscape_16x9_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + landscape_16x9_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += landscape_16x9_column_heading + ' does not have correct object type' + '|'
			landscape_16x9_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if landscape_16x9_valid :
		logger.debug(content_id + ': VALIDATION PASS: ' + landscape_16x9_column_heading + ' (' + str(landscape_16x9) + ') is supplied correctly')
		array_of_successsfully_validated_resources.append(landscape_16x9)
		return True
	else:
		at_least_one_failure = True
		logger.debug(content_id + ': VALIDATION FAILED: ' + landscape_16x9_column_heading + ' (' + str(landscape_16x9) + ') is not supplied correctly')
		return False

def list_tvod_allowed_values ( list ):
	str = ''
	for value in list:
		str = str + value + ','
	str = str[:-1]
	return str

def validate_tvod ( tvod, tvod_column_heading ) :
	global tvod_valid
	global at_least_one_failure
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating TVOD (' + str(tvod) + ') for content_id (' + content_id + ')')

	if isinstance(tvod, unicode) or isinstance(tvod, int) or isinstance(tvod, float) and not np.isnan(tvod):
		logger.debug( tvod_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		if ( tvod_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(tvod):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is having a new line character which is not allowed')
				tvod_valid = False
				failureReasonForThisRow += tvod_column_heading + ' is having a new line character which is not allowed' + '|'
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is not having a new line character. Proceeding with further checks')
		if isinstance(tvod, int):
			logger.debug( content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is a integer value. Proceeding to validate' )
			if ( tvod == 0 or tvod == 1 ) :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is correctly mentioned')
			else:
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') should be one of (' + list_tvod_allowed_values(tvod_allowed_list) + ')')
				failureReasonForThisRow += tvod_column_heading + ' should be one of (' + list_tvod_allowed_values(tvod_allowed_list) + ')' + '|'
				tvod_valid = False
		elif ( isinstance(tvod, float) ):
			logger.debug( content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is a float value. Proceeding to validate' )
			print type(tvod), tvod
			if ( tvod == 0.0 or tvod == 1.0 ) :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is correctly mentioned')
			else:
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') should be one of (' + list_tvod_allowed_values(tvod_allowed_list) + ')')
				failureReasonForThisRow += tvod_column_heading + ' should be one of (' + list_tvod_allowed_values(tvod_allowed_list) + ')' + '|'
				tvod_valid = False
		else:
			tvod = tvod.strip()
			if ( tvod.lower() in ( tvodList.lower for tvodList in tvod_allowed_list ) ):
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is correctly mentioned')
			else:
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') should be one of (' + list_tvod_allowed_values(tvod_allowed_list) + ')')
				failureReasonForThisRow += tvod_column_heading + ' should be one of (' + list_tvod_allowed_values(tvod_allowed_list) + ')' + '|'
				tvod_valid = False
		
		# validate tvod 
		#validate_tvod ( tvod )
		
	else:
		if np.isnan(tvod):
			if ( tvod_column_heading in columns_requiring_validation ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + tvod_column_heading + ' (' + str(tvod) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += tvod_column_heading + ' is mandatory field and must be supplied' + '|'
				title_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + tvod_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type. Its should be one of ( True, False )')
			failureReasonForThisRow += tvod_column_heading + ' does not have correct object type. Its should be one of ( True, False )' + '|'
			tvod_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if tvod_valid :
		logger.debug('VALIDATION PASS: TVOD (' + str(tvod) + ') is supplied correctly for content id (' + content_id + ')')
		return True
	else :
		at_least_one_failure = True
		logger.debug('VALIDATION FAILED: TVOD (' + str(tvod) + ') is supplied correctly for content id (' + content_id + ')')
		return False

def list_valid_quality_rating ( list ):
	str = ''
	for value in list:
		str = str + value + '/'
	str = str[:-1]
	return str

def validate_quality_rating ( quality_rating, quality_rating_column_heading ) :
	global at_least_one_failure
	global quality_rating_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating ' + quality_rating_column_heading + ' (' + str(quality_rating) + ') for content_id (' + content_id + '). Flag is : ' + str(quality_rating_valid))

	if isinstance(quality_rating, unicode):
		quality_rating = str(quality_rating.strip())
		logger.debug( quality_rating_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		
		if quality_rating.lower() not in (qr.lower() for qr in quality_rating_list):
			logger.info('VALIDATION FAILED: ' + quality_rating_column_heading + ' (' + quality_rating + ') for ' + content_id_column_heading + ' (' + content_id + ') should be one of (' + list_valid_quality_rating(quality_rating_list) + ')')
			failureReasonForThisRow += quality_rating_column_heading + ' should be one of (' + list_valid_quality_rating(quality_rating_list) + ') ' + '|'
			quality_rating_valid = False
		else:
			logger.debug('Validation Pass: ' + quality_rating_column_heading + ' (' + quality_rating + ') for ' + content_id_column_heading + ' (' + content_id + ') is correctly suplied and is one of (' + list_valid_quality_rating(quality_rating_list) + ')')
			logger.debug('Till now flag value is : ' + str(quality_rating_valid))

		if ( quality_rating_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(quality_rating):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + quality_rating_column_heading + ' (' + quality_rating + ') is having a new line character which is not allowed')
				failureReasonForThisRow += quality_rating_column_heading + ' is having a new line character which is not allowed' + '|'
				quality_rating_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + quality_rating_column_heading + ' (' + quality_rating + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(quality_rating):
			if ( quality_rating_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + quality_rating_column_heading + ' (' + str(quality_rating) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += quality_rating_column_heading + ' is mandatory field and must be supplied' + '|'
				quality_rating_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + quality_rating_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += quality_rating_column_heading + ' does not have correct object type' + '|'
			quality_rating_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if quality_rating_valid :
		logger.debug('VALIDATION PASS: ' + quality_rating_column_heading + ' (' + str(quality_rating) + ') is supplied correctly for ' + content_id_column_heading + ' (' + content_id + ')')
		return True
	else :
		at_least_one_failure = True
		logger.debug('VALIDATION FAILED: ' + quality_rating_column_heading + ' (' + str(quality_rating) + ') is  not supplied correctly for ' + content_id_column_heading + ' (' + content_id + ')')
		return False

def list_valid_query ( list ):
	str = ''
	for value in list:
		str = str + value + '/'
	str = str[:-1]
	return str

def validate_query ( query, query_column_heading ) :
	global at_least_one_failure
	global query_valid
	global failureReasonForThisRow
	# Debug statement 
	logger.debug('Validating ' + query_column_heading + ' (' + str(query) + ') for content_id (' + content_id + '). Flag is : ' + str(query_valid))

	if isinstance(query, unicode):
		query = str(query.strip())
		logger.debug( query_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') is not null. Proceeding to validate it')
		
		if query.lower() not in (qr.lower() for qr in query_allowed_values):
			logger.info('VALIDATION FAILED: ' + query_column_heading + ' (' + str(query) + ') for ' + content_id_column_heading + ' (' + content_id + ') should be one of (' + list_valid_query(query_allowed_values) + ')')
			failureReasonForThisRow += query_column_heading + ' should be one of (' + list_valid_query(query_allowed_values) + ') ' + '|'
			query_valid = False
		else:
			logger.debug('Validation Pass: ' + query_column_heading + ' (' + query + ') for ' + content_id_column_heading + ' (' + content_id + ') is correctly suplied and is one of (' + list_valid_query(query_allowed_values) + ')')
			logger.debug('Till now flag value is : ' + str(query_valid))

		if ( query_column_heading in columns_to_not_have_newline_characters ) :
			if isPresent_newline_char(query):
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + query_column_heading + ' (' + str(query) + ') is having a new line character which is not allowed')
				failureReasonForThisRow += query_column_heading + ' is having a new line character which is not allowed' + '|'
				query_valid = False
			else :
				logger.debug('VALIDATION PASS: ' + content_id + ': ' + query_column_heading + ' (' + str(query) + ') is not having a new line character. Proceeding with further checks')
	else:
		if np.isnan(query):
			if ( query_column_heading in columns_mandatory_for_ingestion ) :
				logger.info('VALIDATION FAILED: ' + content_id + ': ' + query_column_heading + ' (' + str(query) + ') is mandatory field and must be supplied')
				failureReasonForThisRow += query_column_heading + ' is mandatory field and must be supplied' + '|'
				query_valid = False
		else:
			logger.info('VALIDATION FAILED: ' + query_column_heading + ' for ' + content_id_column_heading + ' (' + content_id + ') does not have correct object type')
			failureReasonForThisRow += query_column_heading + ' does not have correct object type' + '|'
			query_valid = False

	logger.debug("The flag ( at_least_one_failure ) have value : " + str(at_least_one_failure))
	if query_valid :
		logger.debug('VALIDATION PASS: ' + query_column_heading + ' (' + str(query) + ') is supplied correctly for ' + content_id_column_heading + ' (' + content_id + ')')
		return True
	else :
		at_least_one_failure = True
		logger.debug('VALIDATION FAILED: ' + query_column_heading + ' (' + str(query) + ') is  not supplied correctly for ' + content_id_column_heading + ' (' + content_id + ')')
		return False
	
def list_supported_image_formats ( list ):
	str = ''
	for value in list:
		str = str + value + ', '
	str = str[:-1]
	return str

def list_allowed_headers ( list ):
	str = ''
	for value in list:
		str = str + value + ', '
	str = str[:-2]
	return str

def validateCredentialsForS3Upload ( access_key, secret_key, cp_home_directory ):
	temp_s3_session = boto3.Session( aws_access_key_id=access_key, aws_secret_access_key=secret_key )
	temp_s3_client  = temp_s3_session.client('s3')
	try:
		temp_s3_client.list_objects_v2(Bucket=destination_bucket,Prefix=cp_home_directory+'/')
		return True
	except:
		logger.debug('The access key or secret key provided does not seems to be right. Please verify credentials and run script again')
		return False

def uploadFileToS3 ( source, dest_bucket, key ) :
	#temp_s3_session = boto3.Session( aws_access_key_id=access_key, aws_secret_access_key=secret_key )
	#temp_s3_client  = temp_s3_session.client('s3')
	## INITIALIZE BOTO CONNECTION TO AWS S3
	temp_s3_session = boto3.Session( aws_access_key_id=access_key, aws_secret_access_key=secret_key )
	temp_s3_client = temp_s3_session.resource('s3')
	try:
		logger.debug('Uploading file (' + source + ') to S3 bucket (' + dest_bucket + ') having key name (' + key + ')')
		temp_s3_client.meta.client.upload_file ( source, dest_bucket, key)
		return True
	except Exception as e:
		logger.info('FAILED: Uploading file (' + source + ') to S3 bucket (' + dest_bucket + ') having key name (' + key + ')')
		logger.info('Exception : ' + str(e))
		return False

def downloadFileFromS3 ( BUCKET_NAME, BUCKET_FILE_NAME, LOCAL_FILE_NAME ) :
	temp_s3_session = boto3.Session( aws_access_key_id=access_key, aws_secret_access_key=secret_key )
	temp_s3_client  = temp_s3_session.client('s3')
	try :
		temp_s3_client.download_file(BUCKET_NAME, BUCKET_FILE_NAME, LOCAL_FILE_NAME)
		logger.info('Successfully downloaded file (' + LOCAL_FILE_NAME + ') from S3 bucket (' + BUCKET_NAME + ') having key name (' + BUCKET_FILE_NAME + ')')
		return True
	except Exception as e:
		logger.info('FAILED: Downloading file (' + LOCAL_FILE_NAME + ') from S3 bucket (' + BUCKET_NAME + ') having key name (' + BUCKET_FILE_NAME + ')')
		logger.info('Exception : ' + str(e))
		return False

def copyFileFromS3SrcBktToDestBkt (source_json, destBkt, destKey ):
	temp_s3_session = boto3.Session( aws_access_key_id=access_key, aws_secret_access_key=secret_key )
	temp_s3_client = temp_s3_session.resource('s3')
	logger.debug('Copying object from (' + str(source_json) + ') to S3 bucket (' + str(destBkt) + ') having key name (' + str(destKey) + ')')
	try:
		temp_s3_client.meta.client.copy ( source_json, destBkt, destKey)
		return True
	except Exception as e:
		logger.info('FAILED: Uploading file (' + str(source_json) + ') to S3 bucket (' + str(destBkt) + ') having key name (' + str(destKey) + ')')
		logger.info('Exception : ' + str(e))
		return False

def script_usage():
	print('\n\nSCRIPT USAGE:')
	print('-------------')
	print('python ' + str(sys.argv[0]) + ' <ARG1> <ARG2>')
	print('<ARG1> is mandatory. It is the name of the excelsheet that contains content details and its metadata')
	print('       <ARG1> has to be a microsoft excelsheet file')
	print('<ARG2> is optional. It is the name of the config file that contains access key, secret key and home directory of content partner')
	print('       Config file format is :')
	print('       \taccess_key:<access_key>')
	print('       \tsecret_key:<secret_key>')
	print('       \tcp_home_directory:<cp_home_directory>')
	print('       In case <ARG2> is not provided, script will prompt for required details and script will create the config file in current working directory\n\n')

def call_function(column_name):
	switcher = {
		"Content ID": validate_content_id,
		#"ContentID": validate_content_id,
		"Title": validate_title,
		"Content Type": validate_content_type,
		#"ContentType": validate_content_type,
		#"SeasonNumber": validate_season_no,
		"Season Number": validate_season_no,
		#"EpisodeNumber": validate_episode_no,
		"Episode Number": validate_episode_no,
		"Description": validate_description,
		#"Artists": validate_artist,
		"Credits/Cast/Artiste(s)": validate_artist,
		"Genre": validate_genre,
		#"ReleaseDate": validate_release_date,
		"Release Date": validate_release_date,
		"Duration": validate_duration,
		"Language": validate_language,
		#"ContentProducer": validate_content_producer,
		"Content Producer": validate_content_producer,
		#"ParentalGuidanceRating": validate_parental_guide,
		"Parental Guidance Rating": validate_parental_guide,
		"ResourceFile": validate_resource_file,
		"Subtitles": validate_subtitle,
		"Trailer": validate_trailer,
		"Landscape_4x3": validate_landscape_4x3,
		"Portrait_2x3": validate_portrait_2x3,
		"Landscape_16x9": validate_landscape_16x9,
		"TVOD": validate_tvod,
		"Quality Rating": validate_quality_rating,
		"Query": validate_query,
		
		
	}

	return switcher.get(column_name, "nothing")

# Main logic beyond here 
# Initiate the logger
logger = getLogger()

script_config         = ''
filename              = ''
orig_filename         = ''
access_key            = ''
secret_key            = ''

# check command line parameters
# if command line parameter is passed, consider it a file name with access_key, secret_key and filename to process
# if no command line parameter, then ask fo credentials and filename
if ( len(sys.argv) == 1 ):
	logger.info('The script (' + str(sys.argv[0]) + ') has run with no arguments. Exiting the script as excelsheet containing contents and metadata should be passed as argument')
	script_usage()
	sys.exit()
elif ( len(sys.argv) == 2 ):
	filename      = sys.argv[1]
	orig_filename = filename
	script_config = '.script_config'
	dryRun = True
	logger.info('The script (' + str(sys.argv[0]) + ') has run with 1 argument. So considering the first argument as the filename that contains contents. Checking if file (' + str(sys.argv[1]) + ') exists in current working directory')
	
elif ( len(sys.argv) == 3 ):
	filename         = sys.argv[1]
	orig_filename    = filename
	if  sys.argv[2] == 'dryRun=False':
		dryRun = False
		script_config = '.script_config'
	else:
		dryRun = True
		script_config    = sys.argv[2]
	logger.debug('The script (' + str(sys.argv[0]) + ') has run with 2 arguments (' + sys.argv[1] + ',' + sys.argv[2] + '). If second paramete is dryRun=False then consider this arg as dryRun flag. Else consider it config file. If config file, then mandatory paramerter are i.e. access_key, secret_key, cp_folder_in_s3')
elif ( len(sys.argv) == 4 ):
	logger.debug('The script (' + str(sys.argv[0]) + ') has run with 3 arguments (' + str(sys.argv[1]) + ',' + str(sys.argv[2]) + ',' + str(sys.argv[3]) + '). So validating this config file for mandatory paramerter i.e. access_key, secret_key, cp_folder_in_s3')
	filename         = sys.argv[1]
	orig_filename    = filename
	script_config    = sys.argv[2]
	if  sys.argv[3] == 'dryRun=False':
		dryRun = False
	else:
		dryRun = True


if bool(re.search(r"\s", filename.strip())):
	logger.info('FAILED: The input file ( ' + filename + ') should not contain any spaces')
	sys.exit()

try:
	config_fh = open(script_config)
	logger.debug('File (' + script_config + ') exist in directory (' + working_dir + '). Proceeding to validate the file is not a empty file')
except IOError:
	#logger.info('SCRIPT VALIDATION FAILED: File (' + script_config + ') does not exist in directory (' + working_dir + ') or script not able to open it. Please supply the filename / permissions correctly')
	#close config_fh
	logger.info('File (' + script_config + ') does not exist in directory (' + working_dir + ') or script not able to open it. Please answer the following so that script can create the file for you')
	new_config_fh = open(script_config,'w')
	while True:
		access_key = raw_input("Enter access key : ")
		if access_key :
			access_key = access_key.rstrip()
			access_key = access_key.lstrip()
			logger.info('Access key provided by user is (' + access_key + ')')
			logger.debug('Writing access_key to file (' + script_config + ')')
			new_config_fh.write('access_key:' + access_key + '\n')
			break
		else:
			logger.info('Access key not provided. Please Enter access key :')

	while True:
		secret_key = raw_input("Enter secret key : ")
		if secret_key :
			secret_key = secret_key.rstrip()
			secret_key = secret_key.lstrip()
			logger.info('Access key provided by user is (' + secret_key + ')')
			logger.debug('Writing access_key to file (' + script_config + ')')
			new_config_fh.write('secret_key:' + secret_key + '\n')
			break
		else:
			logger.info('Secret key not provided. Please Enter secret key : ')

		
	while True:
		cp_home_directory = raw_input("Enter CP home directory : ")
		if cp_home_directory :
			cp_home_directory = cp_home_directory.rstrip()
			cp_home_directory = cp_home_directory.lstrip()
			logger.info('CP home directory provided by user is (' + cp_home_directory + ')')
			logger.debug('Writing cp_home_directory to file (' + script_config + ')')
			new_config_fh.write('cp_home_directory:' + cp_home_directory + '\n')
			break
		else:
			logger.info('CP home directory not provided. Please Enter CP home directory : ')

	new_config_fh.close()
	config_fh = open(script_config)

if os.stat(script_config).st_size == 0 :
	logger.info('SCRIPT VALIDATION FAILED: File (' + script_config + ') exist in directory (' + working_dir + ') but the file is empty')
	script_usage()
	config_fh.close()
	sys.exit()
else:
	logger.debug('File (' + script_config + ') exists in directory (' + working_dir + ') and is not empty. Extracting access_key, secret_key, cp_home_directory from config file')

found_access_key, found_secret_key, found_cp_home_directory = False, False, False
for config_line in config_fh:
	logger.debug('Processing line ('+ config_line.rstrip() +') from config file ('+ script_config +')')
	if re.match('^access_key:', config_line):
		access_key = config_line.rstrip().split(':')[-1].lstrip()
		if ( access_key ):
			found_access_key = True
			logger.debug('ACCESS KEY VALIDATION PASS: The access_key line (' + config_line.rstrip() + ') is found inside the file (' + script_config + ') and have a value (' + access_key + ')')
		else:
			logger.info('ACCESS KEY VALIDATION FAILED: The access_key line (' + config_line.rstrip() + ') is found inside the file (' + script_config + ') but does not have any value ( empty value )')
			access_key = ''
			found_access_key = False
	elif re.match('^secret_key:', config_line):
		secret_key = config_line.rstrip().split(':')[-1].lstrip()
		if (secret_key):
			found_secret_key = True
			logger.debug('SECRET KEY VALIDATION PASS: The secret_key line (' + config_line.rstrip() + ') is found inside the file (' + script_config + ') and have a value (' + secret_key + ')')
		else:
			logger.info('SECRET KEY VALIDATION FAILED: The secret_key line (' + config_line.rstrip() + ') is found inside the file (' + script_config + ') but does not have any value ( empty value )')
			secret_key=''
			found_secret_key = False

	elif re.match('^cp_home_directory:', config_line):
		cp_home_directory = config_line.rstrip().split(':')[-1].lstrip()
		if cp_home_directory :
			found_cp_home_directory = True
			logger.debug('CP HOME DIR VALIDATION PASS: The cp_home_directory line (' + config_line.rstrip() + ') is found inside the file (' + script_config + ') and have a value (' + cp_home_directory + ')')
		else:
			logger.info('CP HOME DIR VALIDATION: The cp_home_directory line (' + config_line.rstrip() + ') is found inside the file (' + script_config + ') but does not have any value ( empty value )')
			found_cp_home_directory = False

	else:
		logger.debug('The line ('+ config_line.rstrip() +') does not contain any of the keywords (\'access_key\', \'secret_key\' or \'cp_home_directory\') the script is looking for. Looks like this line is not mandatory and can be removed. Skipping this line')
		

if ( found_access_key and found_secret_key and found_cp_home_directory):
	logger.debug('The file ('+ script_config +') contains all the mandatory keywords (\'access_key\', \'secret_key\' or \'cp_home_directory\') the script is looking for. Validating if the credentials and cp directory is valid and have S3 access')
	if ( validateCredentialsForS3Upload ( access_key, secret_key, cp_home_directory ) ) :
		logger.debug('SCRIPT VALIDATION PASS: The credentials have a access to S3 bucket and cp_home_directory. Hence continuing to validate the content in mentioned file ('+ filename +') ')
	else:
		logger.info('SCRIPT VALIDATION FAILED: The credentials does not have a access to S3 bucket (' + destination_bucket + '/' + cp_home_directory + '/ ). The access_key (' + access_key + ') and secret_key (' + secret_key + ') provided seems not correct or there is a network error')
		sys.exit()
else :
	logger.info('SCRIPT VALIDATION FAILED: The file ('+ script_config +') does not contain all of the keywords (\'access_key\', \'secret_key\' or \'cp_home_directory\') the script is looking for OR the values are empty. Hence exiting the script')
	config_fh.close()
	sys.exit()

config_fh.close()

if ( files_location == 's3'):
	if not downloadFileFromS3 ( destination_bucket, cp_home_directory + '/' + filename, filename) :
		logger.info('FAILED: Not able to download the file (' + filename + ') from S3 location (' + destination_bucket + ') for key (' + cp_home_directory + '/' + filename + ')' )
		sys.exit()
	else:
		logger.debug('Successfully downloaded the file (' + filename + ') from S3 location (' + destination_bucket + ') for key (' + cp_home_directory + '/' + filename + ')' )
try:
	file_fh = open(filename)
	logger.debug('File (' + filename + ') exist in current working directory (' + working_dir + '). Validating if credentials are provided properly')
	file_fh.close()
except Exception as e:
	logger.debug("Exception: " + str(e))
	logger.info('FAILED: File (' + filename + ') does not exist in current working directory (' + working_dir + '). Please supply the filename correctly')
	script_usage()
	sys.exit()


	
# Check if the input file is XLSX or CSV
# orig_filename = filename
if ( re.search ( '.csv$', filename ) ):
	logger.debug( 'The input file ( ' + filename + ' ) is a CSV extension. Reading the CSV file from (' + files_location +')')
	try:
		# Read CSV file
		# look at the first ten thousand bytes to guess the character encoding
		#with open(filename, 'rb') as rawdata:
		#	print rawdata
		#    result = chardet.detect(rawdata.read(10000))

		# check what the character encoding might be
		#print(result)
		content = pd.read_csv(filename, encoding = 'iso-8859-1', index_col=False)
		logger.debug('Converting file (' + filename + ' ) from CSV format to xlxs format.')
		content.to_excel (r''+filename+'.xlsx', index = False, header=True)
		logger.debug('Converted file (' + filename + ' ) from CSV format to xlxs format. New file name ( ' + filename + '.xlxs )')
		filename=filename+'.xlsx'
	except Exception as e:
		logger.debug(str(e))
		logger.info( 'The input CSV file can not be opened. May be it is not avaible in the current directory or s3. Please check and re run the script')
		exit()
if ( re.search ( '.xlsx$', filename ) ):
	logger.debug( 'The input file is a XLSX extension. Reading the XLSX file from disk')
	try:
		# Read XLXS file
		content = pd.read_excel(filename)
		logger.debug( 'The input XLSX file is read successfully from disk')
		# Find out total no of rows and columns
		no_of_rows = content.shape[0] + 1
		no_of_columns = content.shape[1]
	except Exception as e:
		logger.debug(str(e))
		logger.info( 'The input XLSX file can not be opened. May be it is not avaible in the current directory. Please check and re run the script')
		exit()


logger.info('Total no of rows: ' + str(no_of_rows) )
logger.info('Total no of columns: ' + str(no_of_columns) )

if ( no_of_columns == 17 or no_of_columns == 18 ):
	old_format_file = True
	if ( no_of_columns == 18 ):
		no_of_columns = 17
	today = parse(now.strftime("%d/%m/%Y"),dayfirst=True)
	expiry_date = parse(datetime.datetime.strptime(old_version_support_date,"%d/%m/%Y").strftime("%d/%m/%Y"), dayfirst=True)

	logger.debug('Today date : ' + str(today) ) 
	logger.debug('Support end date : ' + str(expiry_date) )
	
	if ( today < expiry_date ):
		logger.info("The last day to support old format is (" + old_version_support_date + "). After that, the old version will not be supported. Proceeding to validate the file")
		columns_mandatory_for_ingestion = ('Content ID', 'Title', 'Content Type', 'Season Number', 'Episode Number', 'Description', 'Credits/Cast/Artiste(s)', 'Genre', 'Release Date', 'Duration','Language', 'Content Producer', 'Parental Guidance Rating', 'ResourceFile', 'Landscape_4x3', 'Portrait_2x3', 'Landscape_16x9')
		columns_requiring_validation = ('Content ID', 'Title', 'Content Type', 'Season Number', 'Episode Number', 'Description', 'Credits/Cast/Artiste(s)', 'Genre', 'Release Date', 'Duration','Language', 'Content Producer', 'Parental Guidance Rating', 'ResourceFile', 'Subtitles', 'Landscape_4x3', 'Portrait_2x3', 'Landscape_16x9')
	else:
		logger.info("The last day to support old format was (" + old_version_support_date + ") which is already expired. Not performing any validations. Please get the new file format, fill it up and run agian")
		exit(0)

	
# Validate the column headings are as expected 
validate_column_heading(content.columns, no_of_columns)

overall_failure = False
first_query_column = True

for row in content.values:
	column_sequence_valid, content_id_valid, title_valid, content_type_valid, season_no_valid, episode_no_valid, description_valid, artist_valid, genre_valid, release_date_valid, duration_valid, language_valid, content_producer_valid, parental_guide_valid, resource_file_valid, subtitle_valid, trailer_valid, landscape_4x3_valid, portrait_2x3_valid, landscape_16x9_valid, tvod_valid, quality_rating_valid, query_valid = True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True
	failureReasonForThisRow = ''
	failureRowCSV = []
	if(len(row) != len(columns_mandatory_for_ingestion)):
		logger.info('ERROR: Can not proceed further as the filename provided does not have required no of columns. The columns required are : (' + list_allowed_headers(columns_mandatory_for_ingestion) + ')')
		exit(0)
	if ( first_query_column and not old_format_file) :
		global content_id
		content_id = row[0]
		content_id_column_heading = columns_mandatory_for_ingestion[0]
		user_value_for_query  = row[query_column_no-1]
		column_name_for_query = columns_mandatory_for_ingestion[query_column_no-1]
		logger.debug('Validating column (' + column_name_for_query + ') since flag named (first_query_column) is enabled')
		logger.debug("Processing Column : (" + column_name_for_query + ") and Row : (" + str(user_value_for_query) + ")")
		response = call_function(column_name_for_query)(user_value_for_query,column_name_for_query)
		if ( query_valid ) :
			logger.debug('Since validation is pass for column (' + column_name_for_query + '), assigning appropriate validation steps depending on the value for this column (' + user_value_for_query + ') ')
			if ( user_value_for_query.lower() == 'Ingestion'.lower() ) :
				logger.debug(column_name_for_query + ' header value is (' + user_value_for_query + ') which matched with type (Ingestion)')
				columns_requiring_validation = query_ingestion_list
				logger.debug('Updated column required for validation to the following list (' + list_valid_query(columns_requiring_validation) + ')')
			if ( user_value_for_query.lower() == 'correction_vd'.lower() ) :
				logger.debug(column_name_for_query + ' header value is (' + user_value_for_query + ') which matched with type (correction_vd)')
				columns_requiring_validation = query_correction_vd_list
				logger.debug('Updated column required for validation to the following list (' + list_valid_query(columns_requiring_validation) + ')')
			if ( user_value_for_query.lower() == 'correction_tr'.lower() ) :
				logger.debug(column_name_for_query + ' header value is (' + user_value_for_query + ') which matched with type (correction_tr)')
				columns_requiring_validation = query_correction_tr_list
				logger.debug('Updated column required for validation to the following list (' + list_valid_query(columns_requiring_validation) + ')')
			if ( user_value_for_query.lower() == 'correction_nm'.lower() ) :
				logger.debug(column_name_for_query + ' header value is (' + user_value_for_query + ') which matched with type (correction_nm)')
				columns_requiring_validation = query_correction_nm_list
				logger.debug('Updated column required for validation to the following list (' + list_valid_query(columns_requiring_validation) + ')')
		else:
			logger.info('Not proceeding further as column (' + columns_mandatory_for_ingestion[query_column_no-1] + ') is a mandatory feild. Allowed values are : (' + list_valid_query(query_allowed_values) + ')')
			exit(0)
		#if ( row[query_column_no-1].lower() not in (qr.lower() for qr in query_allowed_values):
		first_query_column = False
		
	logger.debug('Processing Row (' + str(row) + ')')
	for index,column in enumerate(columns_mandatory_for_ingestion):
		failureRowCSV.append(str(row[index]))
		logger.debug("Processing Column : (" + column + ") and Row : (" + str(row[index]) + ")")
		if column in columns_requiring_validation:
			if column in comma_supported_feilds:
				row[index] = row[index].strip()
			response = call_function(column)(row[index],column)
			if ( response == 'skip' ):
				skip_index = index
				while skip_index <= len(columns_mandatory_for_ingestion):
					#failureRowCSV += str(row[index]) + ','
					failureRowCSV.append(str(row[index]))
					skip_index+=1
				#failureRowCSV += 'SKIPPED,' + str(failureReasonForThisRow) + "\n"
				failureRowCSV.append('SKIPPED')
				failureRowCSV.append(str(failureReasonForThisRow))
				array_of_failure_rows.append(failureRowCSV)
				logger.info('VALIDATION FAILED: The column ( ' + column + ' ) value seems to be not provided for row no (' + str(index) + '). Its a mandatory feild and must be provided. Skipping all checks for this row')
				at_least_one_failure = True
				break
		else:
			logger.debug("The column : (" + column + ") is configured to not perform any validation. Hence skipping it. The value : (" + str(row[index]) + ") will not be validated")
	if not first_query_column: first_query_column = True 

	if ( at_least_one_failure ) :
		overall_failure = True
		at_least_one_failure = False
		#failureRowCSV += 'FAILED,' + str(failureReasonForThisRow)
		failureRowCSV.append('FAILED')
		failureRowCSV.append(str(failureReasonForThisRow))
		array_of_failure_rows.append(failureRowCSV)
	else:
		#failureRowCSV += 'PASS,'
		failureRowCSV.append('PASS')
		array_of_failure_rows.append(failureRowCSV)



# Open file to save the success contents and upload to S3
filename_for_successful_rows = orig_filename.split('.')[0] + '_success_' + getCurrentDateTime() + '.csv'
filename_for_failure_rows = orig_filename.split('.')[0] + '_failure_' + getCurrentDateTime() + '.csv'



logger.debug('The validity flags have the following values (' + str(column_sequence_valid) + ',' + str(content_id_valid) + ',' + str(title_valid) + ',' + str(content_type_valid) + ',' + str(season_no_valid) + ',' + str(episode_no_valid) + ',' + str(description_valid) + ',' + str(artist_valid) + ',' + str(genre_valid) + ',' + str(release_date_valid) + ',' + str(duration_valid) + ',' + str(language_valid) + ',' + str(content_producer_valid) + ',' + str(parental_guide_valid) + ',' + str(resource_file_valid) + ',' + str(subtitle_valid) + ',' + str(trailer_valid) + ',' + str(landscape_4x3_valid) + ',' + str(portrait_2x3_valid) + ',' + str(landscape_16x9_valid) + ',' + str(tvod_valid) + ',' + str(quality_rating_valid) +')' )
if not overall_failure:
	global flag_filesInDestBucket
	flag_filesInDestBucket = True

	logger.info('VALIDATION PASS: The provided input file (' + orig_filename + ') is validated and good for further processing')
	
	fH_success = open ( filename_for_successful_rows, 'w+' )
	csv_heading = ''
	for column_heading in columns_mandatory_for_ingestion:
		csv_heading += column_heading + ','
	csv_heading += 'Batch ID'
	fH_success.write(csv_heading + "\n")
	for row in content.values:
		csv_row = ''
		for index,column in enumerate(columns_mandatory_for_ingestion):
			if pd.isnull(row[index]):
				csv_row += ','
			elif column in comma_supported_feilds :
				if re.match('^".*"$', str(row[index])):
					csv_row += str(row[index]) + ','
				else:
					csv_row += '"' + str(row[index]) + '",'
			else:
				csv_row += str(row[index]) + ','
		csv_row += str(random_number)
		fH_success.write(str(csv_row) + "\n")

	fH_success.close()

	if ( not dryRun ) :
		cp_home_directory_ingestion_bucket = cp_home_directory.split('/')[0]
		time.sleep(delayCopy)
		#if ( old_format_file ) :
		if ( False ) : # Commented above line and making sure it does not enter this conditio. As it is decided that contents will go first and then meta csv irrespective of old or new format
			logger.debug('Uploading successfully validated file (' + filename_for_successful_rows + ') for the batch (' + str(random_number) + ')' )
			if uploadFileToS3 ( filename_for_successful_rows, ingestion_trigger_bucket, cp_home_directory_ingestion_bucket + '/'  + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) ):
				logger.debug('Successfully copied validated file (' + filename_for_successful_rows + ') to (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) + ')')
			else:
				flag_filesInDestBucket = False
				logger.debug('FAILED: Could not upload validated file (' + filename_for_successful_rows + ') to (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) + ')')

		for valid_resource in array_of_successsfully_validated_resources:
			source_dict = {
				'Bucket': destination_bucket,
				'Key': cp_home_directory + '/' + valid_resource
			}
			if ( copyFileFromS3SrcBktToDestBkt( source_dict, ingestion_trigger_bucket, cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(valid_resource) ) ):
				logger.debug('Successfully copied resource (' + valid_resource + ') from (' + source_dict['Bucket'] + '/' + source_dict['Key'] + ') to (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(valid_resource) + ')')
			else:
				flag_filesInDestBucket = False
				logger.info('FAILED: Not able to copy resource from ('  + source_dict['Bucket'] + '/' + source_dict['Key'] + ') to (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(valid_resource) + ')')

			time.sleep(delayCopy)
		
		if ( flag_filesInDestBucket ):
			logger.info('Copied all the relevant files to destination bucket (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/)')
		else:
			logger.info('Error copying files to destination bucket (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/). Need to fix and re run the script')

		#if ( not old_format_file ) :
		if ( True ) : # Commented above line and making sure it always enter this condition. As it is decided that contents will go first and then meta csv irrespective of old or new format
			logger.debug('Uploading successfully validated file (' + filename_for_successful_rows + ') for the batch (' + str(random_number) + ')' )
			#if uploadFileToS3 ( filename_for_successful_rows, destination_bucket, cp_home_directory + '/success/'  + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) ):
			if uploadFileToS3 ( filename_for_successful_rows, ingestion_trigger_bucket, cp_home_directory_ingestion_bucket + '/'  + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) ):
				logger.debug('Successfully copied validated file (' + filename_for_successful_rows + ') to (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) + ')')
			else:
				flag_filesInDestBucket = False
				logger.debug('FAILED: Could not upload validated file (' + filename_for_successful_rows + ') to (' + ingestion_trigger_bucket + '/' + cp_home_directory_ingestion_bucket + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(filename_for_successful_rows) + ')')

		os.remove(filename)

		#if ( airtelApproval ):
		#	start_ingestion_dict = {
		#		'Bucket': destination_bucket,
		#		'Key': cp_home_directory + '/' + valid_resource
		#	}
		#	if ( copyFileFromS3SrcBktToDestBkt( source_dict, destination_bucket, cp_home_directory + '/success/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(valid_resource) ) ):
		#		logger.debug('Successfully copied resource (' + valid_resource + ') from (' + source_dict['Bucket'] + '/' + source_dict['Key'] + ') to (' + destination_bucket + '/' + cp_home_directory + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(valid_resource) + ')')
		#	else:
		#		flag_filesInDestBucket = False
		#		logger.info('FAILED: Not able to copy resource from ('  + source_dict['Bucket'] + '/' + source_dict['Key'] + ') to (' + destination_bucket + '/' + cp_home_directory + '/' + str(random_number) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '/' + str(valid_resource) + ')')


	else:
		logger.info('Since  dry run is (' + str(dryRun) + '), not uploading contents and successfully validated file (' + filename_for_successful_rows + ') to s3.' )
else:
	logger.info('VALIDATION FAILED: Some or all the provided fields for input file (' + orig_filename + ') is missing or invalid. Please correct the fields and run the script again')
	#logger.debug('The above failed validation is for row : ' + str(row))
	#fH_failed.write(str(row))
	#fH_failed.write(content_id + ',' + title + ',' + content_type + ',' + str(season_no) + ',' + str(episode_no) + ',' + description + ',' + artist + ',' + genre + ',' + release_date + ',' + duration + ',' + language + ',' + content_producer + ',' + parental_guide + ',' + resource_file + ',' + landscape_4x3 + ',' + potrait_2x3 + ',' + landscape_16x9 + "\n")
	logger.info('######################################################################################################################################')
	logger.info('Please fix the above mentioned failures and retry again. The file (' + orig_filename + ') is not eligible to upload to S3')
	logger.info('######################################################################################################################################')
	logger.debug('Removing file (' + filename_for_successful_rows + ') as there are above mentioned failures')
	if os.path.exists( filename_for_successful_rows ):
		logger.debug('File (' + filename_for_successful_rows + ') exists. Hence deleting it' )
		try:
			os.remove(filename_for_successful_rows)
			logger.debug('File (' + filename_for_successful_rows + ') deleted successfully')
		except:
			logger.debug('File (' + filename_for_successful_rows + ') not able to deleted')

	logger.debug('Created file (' + filename_for_failure_rows + ') as there are failures')

	headers = columns_mandatory_for_ingestion + ('Result','FailureReason')
	header_arr = []
	for header in headers:
		header_arr.append(header)

	with open( filename_for_failure_rows, 'w') as outfile:
	    mywriter = csv.writer(outfile)
	    mywriter.writerow(header_arr)

	    for d in array_of_failure_rows:
	        mywriter.writerow(d)

